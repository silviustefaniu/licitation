export default [
  {
    url: null,
    name: 'Dashboard',
    tag: '2',
    tagColor: 'warning',
    icon: 'HomeIcon',
    i18n: 'Dashboard',
    submenu: [
      {
        url: '/admin/dashboard/analytics',
        name: 'Analytics',
        slug: 'dashboard-analytics',
        i18n: 'Analytics'
      },
      {
        url: '/admin/dashboard/ecommerce',
        name: 'eCommerce',
        slug: 'dashboard-ecommerce',
        i18n: 'eCommerce'
      }
    ]
  },
  {
    header: 'Manage',
    icon: 'PackageIcon',
    i18n: 'Manage',
    items: [
      {
        url: null,
        name: 'Users',
        icon: 'UserIcon',
        i18n: 'Users',
        submenu: [
          {
            url: '/admin/user/user-list',
            name: 'List',
            slug: 'admin-user-list',
            i18n: 'List',
            permission: 'list_user'
          },
          {
            url: '/admin/user/roles',
            name: 'Roles',
            slug: 'admin-user-roles',
            i18n: 'Roles',
            permission: 'list_role'
          }
        ]
      }
    ]
  },
  {
    header: 'Frontend',
    icon: 'LayersIcon',
    i18n: 'Frontend',
    items: [
      {
        url: null,
        name: 'CMS',
        icon: 'LayoutIcon',
        i18n: 'CMS',
        submenu: [
          {
            url: null,
            name: 'Pages',
            icon: 'MenuIcon',
            i18n: 'Pages',
            submenu: [
              {
                url: '/admin/cms/pages/create',
                name: 'Create',
                i18n: 'Create',
                permission: 'create_page',
              },
              {
                url: '/admin/cms/pages',
                name: 'List',
                i18n: 'List',
                permission: 'list_page',
              }
            ]
          },
          {
            url: '/admin/cms/menus',
            name: 'Menus',
            slug: 'cms-menus',
            i18n: 'Menus',
            permission: 'manage_menus'
          }
        ]
      }
    ]
  }
]

