<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Hi {{ $name }},
    <br>
    
    Your request to reset password has been accepted
    <br>
    Please click on the link below or copy it into the address bar of your browser to access the reset password page:
    <br>

    <a href="{{ url('/reset-password/'. $uuid)}}">Reset my password </a>

    <br/>
</div>

</body>
</html>