(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[36],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/core-js/json/stringify */ "./node_modules/@babel/runtime/core-js/json/stringify.js");
/* harmony import */ var _babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _UserEditTabAccount_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserEditTabAccount.vue */ "./resources/js/src/views/apps/user/user-edit/UserEditTabAccount.vue");
/* harmony import */ var _UserEditTabInformation_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./UserEditTabInformation.vue */ "./resources/js/src/views/apps/user/user-edit/UserEditTabInformation.vue");
/* harmony import */ var _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/store/user-management/moduleUserManagement.js */ "./resources/js/src/store/user-management/moduleUserManagement.js");

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

 // Store Module


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    UserEditTabAccount: _UserEditTabAccount_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    UserEditTabInformation: _UserEditTabInformation_vue__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      user_data: {
        id: 0,
        name: 'Full name',
        email: 'name.surname@example.com',
        status: 'offline',
        is_verified: "neverificat",
        user_role: 'user',
        local_roles: [{
          label: 'User',
          value: 'user'
        }, {
          label: 'Manager',
          value: 'Manager'
        }],
        password: '',
        gender: 0,
        active: 0,
        contact_options: ['email']
      },
      user_not_found: false,

      /*
        This property is created for fetching latest data from API when tab is changed
          Please check it's watcher
      */
      activeTab: 0
    };
  },
  watch: {
    activeTab: function activeTab() {}
  },
  methods: {
    save_changes: function save_changes() {
      var _this = this;

      /* eslint-disable */
      if (!this.validateForm) return; // Loading

      this.$vs.loading();
      this.$store.dispatch('userManagement/createUser', this.user_data).then(function () {
        _this.$vs.loading.close();
      }).catch(function (error) {
        _this.$vs.loading.close();

        _this.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });
      });
    },
    reset_data: function reset_data() {
      this.user_data = JSON.parse(_babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default()(this.data));
    },
    validateForm: function validateForm() {
      return !this.errors.any();
    }
  },
  created: function created() {
    // Register Module UserManagement Module
    if (!_store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_3__["default"].isRegistered) {
      this.$store.registerModule('userManagement', _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_3__["default"]);
      _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_3__["default"].isRegistered = true;
    }
  },
  computed: {
    activeUserInfo: function activeUserInfo() {
      return this.$store.state.AppActiveUser;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=template&id=0a10cbd1&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=template&id=0a10cbd1& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-user-new" } },
    [
      _vm.user_data && _vm.activeUserInfo.permissions.includes("create_user")
        ? _c(
            "vx-card",
            [
              _c(
                "div",
                {
                  staticClass: "tabs-container px-6 pt-6",
                  attrs: { slot: "no-body" },
                  slot: "no-body"
                },
                [
                  _c(
                    "vs-tabs",
                    {
                      staticClass: "tab-action-btn-fill-conatiner",
                      model: {
                        value: _vm.activeTab,
                        callback: function($$v) {
                          _vm.activeTab = $$v
                        },
                        expression: "activeTab"
                      }
                    },
                    [
                      _c(
                        "vs-tab",
                        {
                          attrs: {
                            label: "Account",
                            "icon-pack": "feather",
                            icon: "icon-user"
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "tab-text" },
                            [
                              _c("user-edit-tab-account", {
                                staticClass: "mt-4",
                                attrs: { data: _vm.user_data }
                              })
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "vs-tab",
                        {
                          attrs: {
                            label: "Informati",
                            "icon-pack": "feather",
                            icon: "icon-info"
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "tab-text" },
                            [
                              _c("user-edit-tab-information", {
                                staticClass: "mt-4",
                                attrs: { data: _vm.user_data }
                              })
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("vs-divider"),
              _vm._v(" "),
              _c("div", { staticClass: "w-full" }, [
                _c(
                  "div",
                  {
                    staticClass: "mt-8 flex flex-wrap items-center justify-end"
                  },
                  [
                    _c(
                      "vs-button",
                      {
                        staticClass: "ml-auto mt-2",
                        attrs: { disabled: !_vm.validateForm },
                        on: { click: _vm.save_changes }
                      },
                      [_vm._v("Save Changes")]
                    ),
                    _vm._v(" "),
                    _c(
                      "vs-button",
                      {
                        staticClass: "ml-4 mt-2",
                        attrs: { type: "border", color: "warning" },
                        on: { click: _vm.reset_data }
                      },
                      [_vm._v("Reset")]
                    )
                  ],
                  1
                )
              ])
            ],
            1
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/apps/user/user-edit/UserNew.vue":
/*!****************************************************************!*\
  !*** ./resources/js/src/views/apps/user/user-edit/UserNew.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserNew_vue_vue_type_template_id_0a10cbd1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserNew.vue?vue&type=template&id=0a10cbd1& */ "./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=template&id=0a10cbd1&");
/* harmony import */ var _UserNew_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserNew.vue?vue&type=script&lang=js& */ "./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserNew_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserNew_vue_vue_type_template_id_0a10cbd1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserNew_vue_vue_type_template_id_0a10cbd1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/apps/user/user-edit/UserNew.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserNew_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserNew.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserNew_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=template&id=0a10cbd1&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=template&id=0a10cbd1& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserNew_vue_vue_type_template_id_0a10cbd1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserNew.vue?vue&type=template&id=0a10cbd1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/user/user-edit/UserNew.vue?vue&type=template&id=0a10cbd1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserNew_vue_vue_type_template_id_0a10cbd1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserNew_vue_vue_type_template_id_0a10cbd1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);