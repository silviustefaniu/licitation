<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update_profile', function ($user, $user_to_update) {
            return $user->id === $user_to_update->id;
        });

        Gate::define('list_user', function ($user) {
            /*if($user_to_update->isAdmin())
            {
                return false;
            }*/
            if($user->hasPermission('list_user'))
            {
                return true;
            }
            return false;
        });
        Gate::define('create_user', function ($user, $user_to_update) {
            /*if($user_to_update->isAdmin())
            {
                return false;
            }*/
            if($user->hasPermission('create_user'))
            {
                return true;
            }
            return false;
        });
        Gate::define('edit_user', function ($user, $user_to_update) {
            /*if($user_to_update->isAdmin())
            {
                return false;
            }*/
            if($user->hasPermission('edit_user'))
            {
                return true;
            }
            return false;
        });
        Gate::define('delete_user', function ($user, $user_to_update) {
            if($user->isSuperAdmin())
            {
                return true;
            }
            if($user->isAdmin())
            {
                return true;
            }
            if($user_to_update->isSuperAdmin())
            {
                return false;
            }
            if($user_to_update->isAdmin())
            {
                return false; 
            }
            if($user->hasPermission('delete_user'))
            {
                return true;
            }
            return false;
        });
        Gate::define('change_role', function ($user, $user_to_update, $role = null) {
            if($user->isSuperAdmin())
            {
                return true;
            }
            if($user_to_update->isSuperAdmin())
            {
                return false;
            }
            if($user_to_update->isAdmin())
            {
                return false;
            }
            if($user->hasPermission('change_role'))
            {
                return true;
            }
            return false;
        });
        Gate::define('list_role', function ($user) {
            if($user->hasPermission('list_role'))
            {
                return true;
            }
            return false;
        });
        Gate::define('create_role', function ($user) {
            if($user->hasPermission('create_role'))
            {
                return true;
            }
            return false;
        });
        Gate::define('edit_role', function ($user) {
            
            if($user->hasPermission('edit_role'))
            {
                return true;
            }
            return false;
        });

        Gate::define('delete_role', function ($user, $role) {
            if(!$role->isValid())
            {
                return false;
            }
            if($user->hasPermission('delete_role'))
            {
                return true;
            }
            return false;
        });

        Gate::define('list_page', function ($user) {
            //if($permission->canBeDeleted())
            
            if($user->hasPermission('list_page'))
            {
                return true;
            }
            return false;
        });
        Gate::define('create_page', function ($user) {
            //if($permission->canBeDeleted())
            
            if($user->hasPermission('create_page'))
            {
                return true;
            }
            return false;
        });

        Gate::define('edit_page', function ($user) {
            //if($permission->canBeDeleted())
            
            if($user->hasPermission('edit_page'))
            {
                return true;
            }
            return false;
        });
        Gate::define('delete_page', function ($user) {
            //if($permission->canBeDeleted())
            
            if($user->hasPermission('delete_page'))
            {
                return true;
            }
            return false;
        });
        Gate::define('make_visible', function ($user) {
            //if($permission->canBeDeleted())
            
            if($user->hasPermission('make_visible'))
            {
                return true;
            }
            return false;
        });
        Gate::define('manage_menus', function ($user) {
            //if($permission->canBeDeleted())
            
            if($user->hasPermission('manage_menus'))
            {
                return true;
            }
            return false;
        });
    }
}
