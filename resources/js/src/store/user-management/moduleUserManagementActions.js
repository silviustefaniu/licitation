/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {
  createUser({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios.post("/api/users/create", item)
        .then((response) => {
          //commit('ADD_ITEM', Object.assign(item, {id: response.data.id}))
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchUsers ({ commit }, access_token) {
    return new Promise((resolve, reject) => {
      axios.get('/api/users/index')
        .then((response) => {
          commit('SET_USERS', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchUser (context, userId) {
    return new Promise((resolve, reject) => {
      axios.get(`/api/users/${userId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  firstLoginCompanyData ({commit}, data){
    return new Promise((resolve, reject) => {
      axios.post(`/api/first-login-company-data`, data)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  firstLoginPersonalData ({commit}, data){
    return new Promise((resolve, reject) => {
      axios.post(`/api/first-login-personal-data`, data)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  firstLoginRefuse ({commit}){
    return new Promise((resolve, reject) => {
      axios.post(`/api/first-login-refuse`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editUser ({commit}, data){
    return new Promise((resolve, reject) => {
      axios.post(`/api/users/update-info`, data)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editUserAcc ({commit}, data){
    return new Promise((resolve, reject) => {
      axios.post(`/api/users/update-account`, data)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editProfile ({commit}, data){
    return new Promise((resolve, reject) => {
      axios.post(`/api/profile`, data)
        .then((response) => {
          commit('UPDATE_USER_INFO', response.data, {root: true})
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editProfileAcc ({commit}, data){
    return new Promise((resolve, reject) => {
      axios.post(`/api/profile/account`, data)
        .then((response) => {
          commit('UPDATE_USER_INFO', response.data, {root: true})
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  removeRecord ({ commit }, userId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/api/users/${userId}`)
        .then((response) => {
          commit('REMOVE_RECORD', userId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  destroyRecord ({ commit }, userId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/api/users/destroy/${userId}`)
        .then((response) => {
          commit('REMOVE_RECORD', userId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  deleteBulk ({ commit }, ids) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/users/delete-bulk`, {ids: ids})
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  getCsvColumns ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get(`/api/get-csv-columns`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editUserAvatar ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/users/avatar`, data)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editProfileAvatar ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/profile/avatar`, data)
        .then((response) => {
          commit('UPDATE_USER_INFO', response.data, {root: true})
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  removeUserAvatar ({ commit }, id) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/users/remove-avatar`, {id: id})
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  removeProfileAvatar ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/profile/remove-avatar`)
        .then((response) => {
          commit('UPDATE_USER_INFO', response.data, {root: true})
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  }
}
