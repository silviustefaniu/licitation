<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shortcode extends Model
{

    protected $appends = [
        'shortcodes'
    ];

    public function getShortcodesAttribute()
    {
        $siblings = self::whereNotNull('parent')->where('id', '<>', $this->id)->where('parent', $this->id)->get();
        if($siblings === null)
        {
            return null;
        }
        foreach($siblings as &$sibling)
        {
            if($sibling->getShortcodesAttribute() !== null)
            {
                $sibling->siblings = $sibling->getShortcodesAttribute();
            }
        }
        return $siblings;
    }

    public function getContentAttribute()
    {
        return json_decode($this->attributes['content']);
    }
}
