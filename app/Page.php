<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $casts = [
        'metadata' => 'array'
    ];


    public function shortcodes()
    {
        return $this->hasMany('App\Shortcode');
    }
    
    public function menus()
    {
        return $this->hasMany('App\Menu');
    }
}
