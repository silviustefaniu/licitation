(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[30],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/ForgotPassword.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/ForgotPassword.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      email: ''
    };
  },
  methods: {
    send_email: function send_email() {
      var _this = this;

      this.$vs.loading();
      this.$store.dispatch('auth/forgotPassword', this.email).then(function (response) {
        _this.$vs.loading.close();

        _this.$vs.notify({
          title: 'Email sent',
          text: "Check your inbox and follow the instructions to reset your password",
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'success'
        });
      }).catch(function (error) {
        _this.$vs.loading.close();

        _this.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/ForgotPassword.vue?vue&type=template&id=7fcba513&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/ForgotPassword.vue?vue&type=template&id=7fcba513& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "h-screen flex w-full bg-img" }, [
    _c(
      "div",
      { staticClass: "vx-col w-10/12 m-4 p-2 ml-auto mr-auto pagina_simpla" },
      [
        _c("h1", [_vm._v("AI UITAT PAROLA?")]),
        _vm._v(" "),
        _c("div", { staticClass: "contact-formular" }, [
          _c(
            "div",
            { staticClass: "formular lg:w-8/12 md:w-10/12 ml-auto mr-auto" },
            [
              _c("vs-input", {
                staticClass: "w-full formular-entry",
                attrs: {
                  type: "email",
                  label: "INTRODU ADRESA DE EMAIL FOLOSITA LA INREGISTRARE",
                  "icon-no-border": "",
                  "icon-pack": "fa",
                  icon: "fa-user"
                },
                model: {
                  value: _vm.email,
                  callback: function($$v) {
                    _vm.email = $$v
                  },
                  expression: "email"
                }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "clearfix" }),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "my-4 w-full btn-executa",
                  on: { click: _vm.send_email }
                },
                [_vm._v("TRIMITE LINK")]
              ),
              _vm._v(" "),
              _c("input", { staticClass: "hidden", attrs: { type: "submit" } }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "formular-help" },
                [
                  _c("router-link", { attrs: { to: "/faq" } }, [
                    _vm._v("(nu te descurci - vezi indrumari)")
                  ])
                ],
                1
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass:
              "login-shortcuts xl:flex xl:flex-row md:flex-col justify-between"
          },
          [
            _c(
              "div",
              { staticClass: "xl:w-1/2 md:w-full xl:mr-5 md:mr-0" },
              [
                _c("router-link", { attrs: { to: "/register" } }, [
                  _c(
                    "svg",
                    {
                      attrs: {
                        xmlns: "http://www.w3.org/2000/svg",
                        viewBox: "0 0 64 64"
                      }
                    },
                    [
                      _c("title", [
                        _vm._v(
                          "Nu ai cont? Nu e problema, click aici si creaza unul!"
                        )
                      ]),
                      _c(
                        "g",
                        { attrs: { id: "Layer_2", "data-name": "Layer 2" } },
                        [
                          _c("path", {
                            attrs: {
                              d:
                                "M46,60A14,14,0,1,0,32,46,14,14,0,0,0,46,60ZM40,45.47A1.46,1.46,0,0,1,41.47,44h1.09A1.46,1.46,0,0,0,44,42.55V41.46A1.46,1.46,0,0,1,45.48,40h1.07A1.46,1.46,0,0,1,48,41.46v1.09A1.46,1.46,0,0,0,49.46,44h1.09A1.46,1.46,0,0,1,52,45.47v1.07A1.46,1.46,0,0,1,50.55,48H49.46A1.46,1.46,0,0,0,48,49.45v1.09A1.46,1.46,0,0,1,46.54,52H45.48A1.46,1.46,0,0,1,44,50.54V49.45A1.46,1.46,0,0,0,42.56,48H41.47A1.46,1.46,0,0,1,40,46.53Z"
                            }
                          }),
                          _c("path", {
                            attrs: {
                              d:
                                "M11.18,44H24.26a4.7,4.7,0,0,0,4.53-3.25,18.06,18.06,0,0,1,6.84-9.45,1,1,0,0,0-.26-1.75,37.6,37.6,0,0,0-5.71-1.17A1.91,1.91,0,0,1,28,26.48h0a1.9,1.9,0,0,1,1-1.69,11,11,0,1,0-10.06,0,1.92,1.92,0,0,1-.63,3.6A33.75,33.75,0,0,0,7.68,31.45,6.56,6.56,0,0,0,4,37.3,7,7,0,0,0,11.18,44Z"
                            }
                          })
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("span", { staticClass: "login-shortcuts-content" }, [
                    _c("span", { staticClass: "login-shortcuts-tit" }, [
                      _vm._v("Nu ai cont?")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "login-shortcuts-txt" }, [
                      _vm._v("nu e problema, click aici si creaza unul")
                    ])
                  ])
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "xl:w-1/2 md:w-full xl:ml-5 md:ml-0 md:mt-6 xl:mt-0"
              },
              [
                _c("a", { attrs: { href: "recuperare-cont.html" } }, [
                  _c(
                    "svg",
                    {
                      attrs: {
                        xmlns: "http://www.w3.org/2000/svg",
                        viewBox: "0 0 64 64"
                      }
                    },
                    [
                      _c("title", [
                        _vm._v(
                          "Ai uitat datele contului? Contacteaza-ne pentru a recupera contul!"
                        )
                      ]),
                      _c(
                        "g",
                        { attrs: { id: "Layer_2", "data-name": "Layer 2" } },
                        [
                          _c("path", {
                            attrs: {
                              d:
                                "M52.47,16h-.76A3.71,3.71,0,0,0,48,19.71V20a8,8,0,0,1-8,8H24a8,8,0,0,1-8-8v-.3A3.71,3.71,0,0,0,12.29,16h-.76A7.53,7.53,0,0,0,4,23.53V52.47A7.53,7.53,0,0,0,11.53,60H52.47A7.53,7.53,0,0,0,60,52.47V23.53A7.53,7.53,0,0,0,52.47,16ZM25.42,49.58H14.64a2.2,2.2,0,0,1-2.13-2.85,8.27,8.27,0,0,1,5.12-5.3.55.55,0,0,0,.09-1A4.52,4.52,0,0,1,20,32a4.62,4.62,0,0,1,4.5,5,4.51,4.51,0,0,1-2.21,3.41.55.55,0,0,0,.09,1,8.26,8.26,0,0,1,5.16,5.31A2.21,2.21,0,0,1,25.42,49.58Zm16.58,0H34a2,2,0,0,1,0-4h8a2,2,0,0,1,0,4Zm8,0a2,2,0,1,1,2-2A2,2,0,0,1,50,49.58ZM50,40H34a2,2,0,0,1,0-4H50a2,2,0,0,1,0,4Z"
                            }
                          }),
                          _c("rect", {
                            attrs: {
                              x: "20",
                              y: "10",
                              width: "24",
                              height: "14",
                              rx: "5.54",
                              ry: "5.54"
                            }
                          })
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _vm._m(0)
                ])
              ]
            )
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "login-shortcuts-content" }, [
      _c("span", { staticClass: "login-shortcuts-tit" }, [
        _vm._v("Ai uitat datele contului?")
      ]),
      _vm._v(" "),
      _c("span", { staticClass: "login-shortcuts-txt" }, [
        _vm._v("contacteaza-ne pentru a recupera contul")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/front/login/ForgotPassword.vue":
/*!*********************************************************!*\
  !*** ./resources/js/src/front/login/ForgotPassword.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ForgotPassword_vue_vue_type_template_id_7fcba513___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ForgotPassword.vue?vue&type=template&id=7fcba513& */ "./resources/js/src/front/login/ForgotPassword.vue?vue&type=template&id=7fcba513&");
/* harmony import */ var _ForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ForgotPassword.vue?vue&type=script&lang=js& */ "./resources/js/src/front/login/ForgotPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ForgotPassword_vue_vue_type_template_id_7fcba513___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ForgotPassword_vue_vue_type_template_id_7fcba513___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/front/login/ForgotPassword.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/front/login/ForgotPassword.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/src/front/login/ForgotPassword.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ForgotPassword.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/ForgotPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/front/login/ForgotPassword.vue?vue&type=template&id=7fcba513&":
/*!****************************************************************************************!*\
  !*** ./resources/js/src/front/login/ForgotPassword.vue?vue&type=template&id=7fcba513& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_template_id_7fcba513___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ForgotPassword.vue?vue&type=template&id=7fcba513& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/ForgotPassword.vue?vue&type=template&id=7fcba513&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_template_id_7fcba513___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_template_id_7fcba513___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);