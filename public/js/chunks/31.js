(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[31],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/LoginUserZero.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/LoginUserZero.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/store/user-management/moduleUserManagement.js */ "./resources/js/src/store/user-management/moduleUserManagement.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// Store Module

/* harmony default export */ __webpack_exports__["default"] = ({
  computed: {
    activeUserInfo: function activeUserInfo() {
      return this.$store.state.AppActiveUser;
    }
  },
  mounted: function mounted() {
    var _this = this;

    this.$store.dispatch('userManagement/firstLoginRefuse').then(function (response) {
      _this.$vs.loading.close();
    }).catch(function (error) {
      _this.$vs.loading.close();

      _this.$vs.notify({
        title: 'Error',
        text: error.response.data.message,
        iconPack: 'feather',
        icon: 'icon-alert-circle',
        color: 'danger'
      });
    });
  },
  created: function created() {
    // Register Module UserManagement Module
    if (!_store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered) {
      this.$store.registerModule('userManagement', _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_0__["default"]);
      _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered = true;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/LoginUserZero.vue?vue&type=template&id=0d0e35f7&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/LoginUserZero.vue?vue&type=template&id=0d0e35f7& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "h-screen flex w-full bg-img" }, [
    _c("div", { staticClass: "vx-col w-10/12 m-4 p-2 ml-auto mr-auto" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "index-user-shortcuts flex flex-col" }, [
        _c("div", {}, [
          _c(
            "div",
            { staticClass: "index-user-shortcuts-entry" },
            [
              _c("router-link", { attrs: { to: "/" } }, [
                _c(
                  "svg",
                  {
                    attrs: {
                      xmlns: "http://www.w3.org/2000/svg",
                      viewBox: "0 0 64 64"
                    }
                  },
                  [
                    _c("title", [_vm._v("Date personale")]),
                    _c(
                      "g",
                      { attrs: { id: "Layer_2", "data-name": "Layer 2" } },
                      [
                        _c("path", {
                          attrs: {
                            d:
                              "M52.47,16h-.76A3.71,3.71,0,0,0,48,19.71V20a8,8,0,0,1-8,8H24a8,8,0,0,1-8-8v-.3A3.71,3.71,0,0,0,12.29,16h-.76A7.53,7.53,0,0,0,4,23.53V52.47A7.53,7.53,0,0,0,11.53,60H52.47A7.53,7.53,0,0,0,60,52.47V23.53A7.53,7.53,0,0,0,52.47,16ZM25.42,49.58H14.64a2.2,2.2,0,0,1-2.13-2.85,8.27,8.27,0,0,1,5.12-5.3.55.55,0,0,0,.09-1A4.52,4.52,0,0,1,20,32a4.62,4.62,0,0,1,4.5,5,4.51,4.51,0,0,1-2.21,3.41.55.55,0,0,0,.09,1,8.26,8.26,0,0,1,5.16,5.31A2.21,2.21,0,0,1,25.42,49.58Zm16.58,0H34a2,2,0,0,1,0-4h8a2,2,0,0,1,0,4Zm8,0a2,2,0,1,1,2-2A2,2,0,0,1,50,49.58ZM50,40H34a2,2,0,0,1,0-4H50a2,2,0,0,1,0,4Z"
                          }
                        }),
                        _c("rect", {
                          attrs: {
                            x: "20",
                            y: "10",
                            width: "24",
                            height: "14",
                            rx: "5.54",
                            ry: "5.54"
                          }
                        })
                      ]
                    )
                  ]
                ),
                _vm._v(" "),
                _c("span", { staticClass: "menu-listing-user-r" }, [
                  _c("span", { staticClass: "menu-listing-user-tit" }, [
                    _vm._v(
                      "\n                                    Date personale\n                                "
                    )
                  ]),
                  _vm._v(" "),
                  _c("span", { staticClass: "menu-listing-user-txt" }, [
                    _vm._v(
                      "\n                                    Informatii despre tine\n                                "
                    )
                  ])
                ])
              ])
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("div", {}, [
          _c(
            "div",
            { staticClass: "index-user-shortcuts-entry" },
            [
              _c("router-link", { attrs: { to: "/" } }, [
                _c(
                  "svg",
                  {
                    attrs: {
                      xmlns: "http://www.w3.org/2000/svg",
                      viewBox: "0 0 64 64"
                    }
                  },
                  [
                    _c("title", [_vm._v("Date companie")]),
                    _c(
                      "g",
                      { attrs: { id: "Layer_3", "data-name": "Layer 3" } },
                      [
                        _c("path", {
                          attrs: {
                            d:
                              "M22.68,8A6.47,6.47,0,0,0,16,14.21a2,2,0,0,0,4,0A2.48,2.48,0,0,1,22.68,12H41.32A2.48,2.48,0,0,1,44,14.21a2,2,0,0,0,4,0A6.47,6.47,0,0,0,41.32,8Z"
                          }
                        }),
                        _c("path", {
                          attrs: {
                            d:
                              "M6.09,28H57.91A2.09,2.09,0,0,0,60,25.91V24.26A4.26,4.26,0,0,0,55.74,20H8.26A4.26,4.26,0,0,0,4,24.26v1.64A2.09,2.09,0,0,0,6.09,28Z"
                          }
                        }),
                        _c("path", {
                          attrs: {
                            d:
                              "M8.26,60H55.74A4.26,4.26,0,0,0,60,55.74V33.85A1.85,1.85,0,0,0,58.15,32H39.75A1.75,1.75,0,0,0,38,33.75,6.17,6.17,0,0,1,32.73,40,6,6,0,0,1,26,34v-.25A1.75,1.75,0,0,0,24.25,32H5.85A1.85,1.85,0,0,0,4,33.85V55.74A4.26,4.26,0,0,0,8.26,60Z"
                          }
                        }),
                        _c("path", {
                          attrs: {
                            d:
                              "M31.54,32.06a1.48,1.48,0,0,0-1.48,1.48V34A2,2,0,0,0,34,34v-.49a1.48,1.48,0,0,0-1.48-1.48Z"
                          }
                        })
                      ]
                    )
                  ]
                ),
                _vm._v(" "),
                _c("span", { staticClass: "menu-listing-user-r" }, [
                  _c("span", { staticClass: "menu-listing-user-tit" }, [
                    _vm._v(
                      "\n                                    Date companie\n                                "
                    )
                  ]),
                  _vm._v(" "),
                  _c("span", { staticClass: "menu-listing-user-txt" }, [
                    _vm._v(
                      "\n                                    Informatii despre compania ta\n                                "
                    )
                  ])
                ])
              ])
            ],
            1
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "index-user-titlu" }, [
      _c("img", {
        staticClass: "idex-user-titlu-icon",
        attrs: {
          src: "images/profile/cont-b.svg",
          title: "Salut, Ionut!",
          alt: "Salut, Ionut!"
        }
      }),
      _vm._v(" "),
      _c("h1", [_vm._v("Salut")]),
      _vm._v(" "),
      _c("span", [
        _vm._v(
          "\n\t\t\t\t\tpentru a intreprinde anumite actiuni, precum creare sau participarea la o licitatie, va fi nevoie sa completezi datele profilului\n\t\t\t\t"
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/front/login/LoginUserZero.vue":
/*!********************************************************!*\
  !*** ./resources/js/src/front/login/LoginUserZero.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _LoginUserZero_vue_vue_type_template_id_0d0e35f7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./LoginUserZero.vue?vue&type=template&id=0d0e35f7& */ "./resources/js/src/front/login/LoginUserZero.vue?vue&type=template&id=0d0e35f7&");
/* harmony import */ var _LoginUserZero_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LoginUserZero.vue?vue&type=script&lang=js& */ "./resources/js/src/front/login/LoginUserZero.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _LoginUserZero_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _LoginUserZero_vue_vue_type_template_id_0d0e35f7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _LoginUserZero_vue_vue_type_template_id_0d0e35f7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/front/login/LoginUserZero.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/front/login/LoginUserZero.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/front/login/LoginUserZero.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginUserZero_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./LoginUserZero.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/LoginUserZero.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginUserZero_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/front/login/LoginUserZero.vue?vue&type=template&id=0d0e35f7&":
/*!***************************************************************************************!*\
  !*** ./resources/js/src/front/login/LoginUserZero.vue?vue&type=template&id=0d0e35f7& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginUserZero_vue_vue_type_template_id_0d0e35f7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./LoginUserZero.vue?vue&type=template&id=0d0e35f7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/LoginUserZero.vue?vue&type=template&id=0d0e35f7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginUserZero_vue_vue_type_template_id_0d0e35f7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginUserZero_vue_vue_type_template_id_0d0e35f7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagement.js":
/*!************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagement.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _moduleUserManagementState_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moduleUserManagementState.js */ "./resources/js/src/store/user-management/moduleUserManagementState.js");
/* harmony import */ var _moduleUserManagementMutations_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./moduleUserManagementMutations.js */ "./resources/js/src/store/user-management/moduleUserManagementMutations.js");
/* harmony import */ var _moduleUserManagementActions_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./moduleUserManagementActions.js */ "./resources/js/src/store/user-management/moduleUserManagementActions.js");
/* harmony import */ var _moduleUserManagementGetters_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./moduleUserManagementGetters.js */ "./resources/js/src/store/user-management/moduleUserManagementGetters.js");
/*=========================================================================================
  File Name: moduleUserManagement.js
  Description: Calendar Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/




/* harmony default export */ __webpack_exports__["default"] = ({
  isRegistered: false,
  namespaced: true,
  state: _moduleUserManagementState_js__WEBPACK_IMPORTED_MODULE_0__["default"],
  mutations: _moduleUserManagementMutations_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  actions: _moduleUserManagementActions_js__WEBPACK_IMPORTED_MODULE_2__["default"],
  getters: _moduleUserManagementGetters_js__WEBPACK_IMPORTED_MODULE_3__["default"]
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementActions.js":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementActions.js ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/core-js/promise */ "./node_modules/@babel/runtime/core-js/promise.js");
/* harmony import */ var _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _axios_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/axios.js */ "./resources/js/src/axios.js");


/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

/* harmony default export */ __webpack_exports__["default"] = ({
  createUser: function createUser(_ref, item) {
    var commit = _ref.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/create", item).then(function (response) {
        //commit('ADD_ITEM', Object.assign(item, {id: response.data.id}))
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchUsers: function fetchUsers(_ref2, access_token) {
    var commit = _ref2.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get('/api/users/index').then(function (response) {
        commit('SET_USERS', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchUser: function fetchUser(context, userId) {
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/users/".concat(userId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  firstLoginCompanyData: function firstLoginCompanyData(_ref3, data) {
    var commit = _ref3.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/first-login-company-data", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  firstLoginPersonalData: function firstLoginPersonalData(_ref4, data) {
    var commit = _ref4.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/first-login-personal-data", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  firstLoginRefuse: function firstLoginRefuse(_ref5) {
    var commit = _ref5.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/first-login-refuse").then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editUser: function editUser(_ref6, data) {
    var commit = _ref6.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/update-info", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editUserAcc: function editUserAcc(_ref7, data) {
    var commit = _ref7.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/update-account", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editProfile: function editProfile(_ref8, data) {
    var commit = _ref8.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/profile", data).then(function (response) {
        commit('UPDATE_USER_INFO', response.data, {
          root: true
        });
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editProfileAcc: function editProfileAcc(_ref9, data) {
    var commit = _ref9.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/profile/account", data).then(function (response) {
        commit('UPDATE_USER_INFO', response.data, {
          root: true
        });
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeRecord: function removeRecord(_ref10, userId) {
    var commit = _ref10.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].delete("/api/users/".concat(userId)).then(function (response) {
        commit('REMOVE_RECORD', userId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  destroyRecord: function destroyRecord(_ref11, userId) {
    var commit = _ref11.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].delete("/api/users/destroy/".concat(userId)).then(function (response) {
        commit('REMOVE_RECORD', userId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteBulk: function deleteBulk(_ref12, ids) {
    var commit = _ref12.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/delete-bulk", {
        ids: ids
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getCsvColumns: function getCsvColumns(_ref13) {
    var commit = _ref13.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/get-csv-columns").then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editUserAvatar: function editUserAvatar(_ref14, data) {
    var commit = _ref14.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/avatar", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editProfileAvatar: function editProfileAvatar(_ref15, data) {
    var commit = _ref15.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/profile/avatar", data).then(function (response) {
        commit('UPDATE_USER_INFO', response.data, {
          root: true
        });
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeUserAvatar: function removeUserAvatar(_ref16, id) {
    var commit = _ref16.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/remove-avatar", {
        id: id
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeProfileAvatar: function removeProfileAvatar(_ref17) {
    var commit = _ref17.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/profile/remove-avatar").then(function (response) {
        commit('UPDATE_USER_INFO', response.data, {
          root: true
        });
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementGetters.js":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementGetters.js ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarGetters.js
  Description: Calendar Module Getters
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementMutations.js":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementMutations.js ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  SET_USERS: function SET_USERS(state, users) {
    state.users = users;
  },
  REMOVE_RECORD: function REMOVE_RECORD(state, itemId) {
    var userIndex = state.users.findIndex(function (u) {
      return u.id === itemId;
    });
    state.users.splice(userIndex, 1);
  }
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementState.js":
/*!*****************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementState.js ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarState.js
  Description: Calendar Module State
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  users: []
});

/***/ })

}]);