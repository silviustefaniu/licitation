<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if($user === null)
        {
            return middlewareError();
        }
        
        if (in_array($user->current_role, Role::ADMINS)) 
        {
            return $next($request);
        }
        return middlewareError();

    }
}
