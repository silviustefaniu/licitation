<?php

namespace App\Http\Middleware;

use Closure;

class IsEmailVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if($user->email_verified_at === null)
        {
            return middlewareError();
        }
        return $next($request);
    }
}
