import Vue from 'vue'
import { AclInstaller, AclCreate, AclRule } from 'vue-acl'
import router from '@/router'

Vue.use(AclInstaller)

let initialRole = 'public'

const userInfo = JSON.parse(localStorage.getItem('userInfo'))
if (userInfo && userInfo.acl_role) initialRole = userInfo.acl_role

export default new AclCreate({
  initial  : initialRole,
  notfound : '/not-authorized',
  router,
  acceptLocalRules : true,
  globalRules: {
    public  : new AclRule('public').or('admin').or('manager').or('user').or('verified').generate(),
    user  : new AclRule('user').generate(),
    verified  : new AclRule('verified').or('admin').or('manager').generate(),
    admin  : new AclRule('admin').generate(),
    manager : new AclRule('manager').or('admin').generate()
  }
})
