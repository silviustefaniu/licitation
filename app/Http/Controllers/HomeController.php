<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Page;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app');
    }

    public function getMenus()
    {
        $menus = Menu::where(function($q){
            return $q->whereHas('page', function($qx){
                return $qx->where('visible', 1);
            })->orWhereNull("page_id");
        })->where(function($q){
            $q->where('parent',0)->orWhereNull('parent');
        })->orderBy('position')->get()->groupBy('type');
        return response()->json($menus);
    }

    public function getPageContent(Request $request)
    {
        $url =  $request->url;
        $page = Page::with('shortcodes')->where('visible', 1)->where('url',$url)->first();
        abort_if($page === null, 404);
        return response()->json($page);
    }
}
