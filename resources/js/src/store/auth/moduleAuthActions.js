import jwt from '../../http/requests/auth/jwt/index.js'
import router from '@/router'
import axios from '../../axios.js'
import { resolve } from 'core-js/fn/promise'

export default {
  loginAttempt ({ dispatch }, payload) {

    // New payload for login action
    const newPayload = {
      userDetails: payload.userDetails,
      notify: payload.notify,
      closeAnimation: payload.closeAnimation
    }

    // If remember_me is enabled change firebase Persistence
    if (payload.checkbox_remember_me) {
      // Try to login
      dispatch('login', newPayload)
    } 
  },
  /*login ({ commit, state, dispatch }, payload) {

    // If user is already logged in notify and exit
    if (state.isUserLoggedIn()) {
      // Close animation if passed as payload
      if (payload.closeAnimation) payload.closeAnimation()

      payload.notify({
        title: 'Login Attempt',
        text: 'You are already logged in!',
        iconPack: 'feather',
        icon: 'icon-alert-circle',
        color: 'warning'
      })

      return false
    }
    axios.post('api/auth/login', {
      email: payload.userDetails.email,
      password: payload.userDetails.password
    }).then((result) => {

    }, (err) => {

      // Close animation if passed as payload
      if (payload.closeAnimation) payload.closeAnimation()

      payload.notify({
        time: 2500,
        title: 'Error',
        text: err.message,
        iconPack: 'feather',
        icon: 'icon-alert-circle',
        color: 'danger'
      })
    })
  },*/
  updateUsername ({ commit }, payload) {
    payload.user.updateProfile({
      name: payload.name
    }).then(() => {

      // If username update is success
      // update in localstorage
      const newUserData = Object.assign({}, payload.user.providerData[0])
      newUserData.name = payload.name
      commit('UPDATE_USER_INFO', newUserData, {root: true})

      // If reload is required to get fresh data after update
      // Reload current page
      if (payload.isReloadRequired) {
        router.push(router.currentRoute.query.to || '/')
      }
    }).catch((err) => {
      payload.notify({
        time: 8800,
        title: 'Error',
        text: err.message,
        iconPack: 'feather',
        icon: 'icon-alert-circle',
        color: 'danger'
      })
    })
  },


  // JWT
  loginJWT ({ commit }, payload) {

    return new Promise((resolve, reject) => {
      jwt.login(payload.userDetails.email, payload.userDetails.password)
        .then(response => {

          // If there's user data in response
          if (response.data.user) {
            // Navigate User to homepage
            // router.push(router.currentRoute.query.to || '/')

            // Set access_token
            localStorage.setItem('access_token', response.data.access_token)

            // Update user details
            commit('UPDATE_USER_INFO', response.data.user, {root: true})

            // Set bearer token in axios
            commit('SET_BEARER', response.data.access_token)

            resolve(response)
          } else {
            reject({message: 'Wrong Email or Password'})
          }

        })
        .catch(error => { reject(error) })
    })
  },
  registerUserJWT ({ commit }, payload) {

    const {  email, password, confirmPassword } = payload.userDetails

    return new Promise((resolve, reject) => {

      // Check confirm password
      if (password !== confirmPassword) {
        reject({message: 'Password doesn\'t match. Please try again.'})
      }

      jwt.registerUser(email, password)
        .then(response => {
          // Redirect User
          //router.push(router.currentRoute.query.to || '/')
          
          // Update data in localStorage
          localStorage.setItem('access_token', response.data.access_token)
          commit('UPDATE_USER_INFO', response.data.user, {root: true})

          resolve(response)
        })
        .catch(error => { reject(error) })
    })
  },
  fetchAccessToken () {
    return new Promise((resolve) => {
      jwt.refreshToken().then(response => { resolve(response) })
    })
  },
  verifyEmail ({ commit }, code) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/auth/verify`, {code: code})
        .then((response) => {
          
            resolve(response)
          
        })
        .catch(error => { reject(error) })
    })
  },
  sendSmsCode ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/auth/send-sms`, {phone: data.phone})
        .then((response) => {
          resolve(response)
        })
        .catch(error => { reject(error) })
    })
  },
  verifyPhone ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/auth/verify-phone`, {sms_code: data.sms_code})
        .then((response) => {
          resolve(response)
        })
        .catch(error => { reject(error) })
    })
  },
  resendEmail ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/auth/resend`, {email: data.email})
        .then((response) => {
          resolve(response)
        })
        .catch(error => { reject(error) })
    })
  },
  forgotPassword ({ commit }, email) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/auth/forgot-password`, {email: email})
        .then((response) => {
          resolve(response)
        })
        .catch(error => { reject(error) })
    })
  },
  
  resetPassword ({ commit }, uuid) {
    return new Promise((resolve, reject) => {
      axios.get(`/api/auth/reset-password/` + uuid)
        .then((response) => {
          resolve(response)
        })
        .catch(error => { reject(error) })
    })
  },
  resetPasswordPost ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/auth/reset-password/`, {email: data.email, uuid: data.uuid, password: data.password})
        .then((response) => {
          resolve(response)
        })
        .catch(error => { reject(error) })
    })
  },

  
  fetchUser ({ commit }, token) {
    return new Promise((resolve, reject) => {
      axios.get(`/api/auth/user/`+token)
      .then((response) => {
          localStorage.setItem('access_token', response.data.access_token)
          commit('UPDATE_USER_INFO', response.data.user, {root: true})
          resolve(response)
        })
        .catch((error) => { 
          reject(error) 
        })
    })
  },

}
