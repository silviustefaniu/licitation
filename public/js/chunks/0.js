(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./resources/js/src/store/cms/moduleCms.js":
/*!*************************************************!*\
  !*** ./resources/js/src/store/cms/moduleCms.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _moduleCmstState_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moduleCmstState.js */ "./resources/js/src/store/cms/moduleCmstState.js");
/* harmony import */ var _moduleCmsMutations_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./moduleCmsMutations.js */ "./resources/js/src/store/cms/moduleCmsMutations.js");
/* harmony import */ var _moduleCmsActions_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./moduleCmsActions.js */ "./resources/js/src/store/cms/moduleCmsActions.js");
/* harmony import */ var _moduleCmsGetters_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./moduleCmsGetters.js */ "./resources/js/src/store/cms/moduleCmsGetters.js");
/*=========================================================================================
  File Name: moduleUserManagement.js
  Description: Calendar Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/




/* harmony default export */ __webpack_exports__["default"] = ({
  isRegistered: false,
  namespaced: true,
  state: _moduleCmstState_js__WEBPACK_IMPORTED_MODULE_0__["default"],
  mutations: _moduleCmsMutations_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  actions: _moduleCmsActions_js__WEBPACK_IMPORTED_MODULE_2__["default"],
  getters: _moduleCmsGetters_js__WEBPACK_IMPORTED_MODULE_3__["default"]
});

/***/ }),

/***/ "./resources/js/src/store/cms/moduleCmsActions.js":
/*!********************************************************!*\
  !*** ./resources/js/src/store/cms/moduleCmsActions.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/core-js/promise */ "./node_modules/@babel/runtime/core-js/promise.js");
/* harmony import */ var _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _axios_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/axios.js */ "./resources/js/src/axios.js");


/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

/* harmony default export */ __webpack_exports__["default"] = ({
  fetchFrontMenus: function fetchFrontMenus(_ref) {
    var commit = _ref.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/home/menus").then(function (response) {
        commit('SET_FRONT_MENUS', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchFrontPage: function fetchFrontPage(_ref2, url) {
    var commit = _ref2.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/home/pages/" + url).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchPages: function fetchPages(_ref3) {
    var commit = _ref3.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/pages/index").then(function (response) {
        commit('SET_PAGES', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchPage: function fetchPage(_ref4, pageId) {
    var commit = _ref4.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/pages/".concat(pageId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchMenus: function fetchMenus(_ref5) {
    var commit = _ref5.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/menus/index").then(function (response) {
        commit('SET_MENUS', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchMenu: function fetchMenu(_ref6, menuId) {
    var commit = _ref6.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/menus/".concat(menuId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  addMenu: function addMenu(_ref7, menu) {
    var commit = _ref7.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/menus/create", {
        name: menu.name,
        url: menu.url,
        blank: menu.blank
      }).then(function (response) {
        //commit('ADD_MENU', response.data)
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  addPage: function addPage(_ref8, item) {
    var commit = _ref8.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/pages/create", {
        name: item.name,
        url: item.url,
        metadata: item.metadata,
        parent: item.parent,
        shortcodes: item.shortcodes,
        visible: item.visible,
        classes: null
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editPage: function editPage(_ref9, item) {
    var commit = _ref9.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/pages/edit", {
        id: item.id,
        name: item.name,
        url: item.url,
        metadata: item.metadata,
        shortcodes: item.shortcodes,
        visible: item.visible,
        classes: null
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeMenu: function removeMenu(_ref10, id) {
    var commit = _ref10.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/menus/delete", {
        id: id
      }).then(function (response) {
        commit('REMOVE_MENU', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removePage: function removePage(_ref11, id) {
    var commit = _ref11.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/pages/delete", {
        id: id
      }).then(function (response) {
        commit('REMOVE_PAGE', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  pageVisibility: function pageVisibility(_ref12, menu) {
    var commit = _ref12.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/pages/make-visible", {
        id: menu.id,
        visible: menu.visible
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  arrangeMenus: function arrangeMenus(_ref13, data) {
    var commit = _ref13.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/menus/arrange", {
        data: data
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  addShortcode: function addShortcode(_ref14, item) {
    var commit = _ref14.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/shortcodes/store", {
        name: item.name,
        classes: item.classes,
        content: item.content,
        position: item.position,
        parent: item.parent,
        visible: item.visible,
        type: item.type
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editShortcode: function editShortcode(_ref15, item) {
    var commit = _ref15.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/shortcodes/edit", {
        id: item.id,
        name: item.name,
        classes: item.classes,
        content: item.content,
        position: item.position,
        parent: item.parent,
        visible: item.visible,
        type: item.type
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeShortcode: function removeShortcode(_ref16, id) {
    var commit = _ref16.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].delete("/api/shortcodes/delete", {
        id: id
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  arrangeShortcodes: function arrangeShortcodes(_ref17, data) {
    var commit = _ref17.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].delete("/api/shortcodes/arrange", {
        data: data
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  uploadShortcode: function uploadShortcode(_ref18, data) {
    var commit = _ref18.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/shortcodes/upload", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ "./resources/js/src/store/cms/moduleCmsGetters.js":
/*!********************************************************!*\
  !*** ./resources/js/src/store/cms/moduleCmsGetters.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarGetters.js
  Description: Calendar Module Getters
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./resources/js/src/store/cms/moduleCmsMutations.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/store/cms/moduleCmsMutations.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  SET_MENUS: function SET_MENUS(state, menus) {
    state.menus.header = menus.header;
    state.menus.footer = menus.footer;
  },
  SET_PAGES: function SET_PAGES(state, pages) {
    state.pages = pages;
  },
  SET_FRONT_MENUS: function SET_FRONT_MENUS(state, menus) {
    state.front_menus = menus;
  },
  ADD_MENU: function ADD_MENU(state, data) {
    if (typeof state.menus.header == "undefined" || state.menus.header.length == 0) {
      state.menus.header = [];
    }

    state.menus.header.push(data);
  },
  REMOVE_MENU: function REMOVE_MENU(state, data) {
    var itemId = data.id;
    var parentId = data.parent;
    var type = data.type; // HERE THE MAIN MENU IS DELETED IN PROMISE. SEE NESTEDMENUS.VUE

    /*if(parentId == null || parentId == 0)
    {
      if(type == 'footer')
      {
        let menuIndex = state.menus.footer.findIndex((u) => u.id == itemId)
        state.menus.footer.splice(menuIndex, 1)
      }
      else
      {
        let menuIndex = state.menus.header.findIndex((u) => u.id == itemId)
        state.menus.header.splice(menuIndex, 1)
      }
    }*/

    if (parentId != null && parentId != 0) {
      if (type == 'footer') {
        state.menus.footer.forEach(function (m) {
          if (m.id == parentId) {
            m.siblings = m.siblings.filter(function (s) {
              return s.id != itemId;
            });
          }
        });
      } else {
        state.menus.header.forEach(function (m) {
          if (m.id == parentId) {
            m.siblings = m.siblings.filter(function (s) {
              return s.id != itemId;
            });
          }
        });
      }
    }
  },
  REMOVE_PAGE: function REMOVE_PAGE(state, page) {
    var itemId = page.id;
    var pageIndex = state.pages.findIndex(function (u) {
      return u.id == itemId;
    });
    state.pages.splice(pageIndex, 1);
  }
});

/***/ }),

/***/ "./resources/js/src/store/cms/moduleCmstState.js":
/*!*******************************************************!*\
  !*** ./resources/js/src/store/cms/moduleCmstState.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarState.js
  Description: Calendar Module State
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  menus: []
});

/***/ })

}]);