// axios
import axios from 'axios'

const baseURL = ''

let withCredentials = false;

const token = document.head.querySelector('meta[name="csrf-token"]');

const headers = {
   'X-CSRF-TOKEN': token.content,
   'Access-Control-Allow-Origin': '*',
   'X-Requested-With': 'XMLHttpRequest',
   'Content-Type': 'application/json',
};


export default axios.create({
    baseURL,
    withCredentials: withCredentials,
    headers: headers
});
