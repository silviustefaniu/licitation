<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Util\Utils;
use Response;
use Mail;
use Carbon\Carbon;
use Gate;
use File;
use Excel;
use \App\Exports\UserExport;

class AdminController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('admin');
    }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexUsers(Request $request)
    {
        abort_unless(Gate::allows('list_user', auth()->user()), 403, 'Gate');
        if(auth()->user()->isSuperAdmin())
        {
            return User::with('roles')->withTrashed()->get();
        }
        else if(auth()->user()->isAdmin())
        {
            return User::with('roles')->withTrashed()->notSuperAdmin()->get();
        }
        else
        {
            return User::with('roles')->withTrashed()->notAdmin()->get();
        }
    }

    public function showFormUser(Request $request)
    {
        return User::withTrashed()->find($request->id);
    }

    public function updateUser(Request $request)
    {
        $user = User::withTrashed()->find($request->id);
        abort_unless(Gate::allows('edit_user', auth()->user(), $user), 403, 'Gate');
        $user->contact_options = $request->contact_options;
        $user->gender = $request->gender;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->region = $request->region;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->address_alt = $request->address_alt;
        $user->iban = $request->iban;
        $user->bank = $request->bank;
        $user->business_name = $request->business_name;
        $user->website = $request->website;
        $user->commerce_no = $request->commerce_no;
        $user->cui = $request->cui;
        $user->name = $request->name;
        $user->email = $request->email;
        $role = Role::where('name', $request->user_role)->first();
        
        if($role === null)
        {
            $role = Role::where('name','user')->first();;
        }
            $user->roles()->sync([]); //one role force
            $user->roles()->attach($role);
            $user->current_role = $role->id;
        

        switch($request->is_verified)
        {
            case 'banned': $user->deleted_at = Carbon::now(); break;
            case 'neverificat': 
                $user->deleted_at = null;
                User::sendEmailVerification($user);
                break;
            case 'verificat': $user->deleted_at = null; 
                            $user->email_verified_at = Carbon::now();  break;
            default: $user->deleted_at = null; break;
        }

        if($user->save())
        {
            return response()->json(true, 200);
        }
        return response()->json(false, 400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        abort_unless(Gate::allows('create_user', auth()->user(), $user), 403, 'Gate');
        $user->contact_options = $request->contact_options;
        $user->gender = $request->gender;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->region = $request->region;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->address_alt = $request->address_alt;
        $user->iban = $request->iban;
        $user->bank = $request->bank;
        $user->business_name = $request->business_name;
        $user->website = $request->website;
        $user->commerce_no = $request->commerce_no;
        $user->cui = $request->cui;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $role = Role::where('name', $request->user_role)->first();
        $user->save();
        if($role === null)
        {
            $role = Role::where('name','user')->first();
        }
        if(Gate::allows('change_role', auth()->user(), $user, $role))
        {
            $user->roles()->sync([]);
            $user->roles()->attach($role);
        }

        switch($request->is_verified)
        {
            case 'verificat':   $user->email_verified_at = Carbon::now();  break;
            default: 
                $user->email_verified_at = null; 
                User::sendEmailVerification($user);
            break;
        }

        if($user->id)
        {
            return response()->json(true, 200);
        }
        return response()->json(false, 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function deleteUser($userId)
    {
        $user = User::with('roles')->where('id', $userId)->first();
        
        abort_unless(Gate::allows('delete_user', auth()->user(), $user), 403, 'Gate');
        
        $user->forceDelete();

        return response()->json(null, 204);
    }

    public function deleteBulk(Request $request)
    {
        $abort = [];
        $ids = collect($request->ids)->pluck('id')->toArray();
        foreach($ids as $id)
        {
            $user = User::with('roles')->where('id', $id)->first();
            if($user !== null && Gate::allows('delete_user', auth()->user(), $user))
            {
                $user->forceDelete();
            }
            else
            {
                $abort[] = $id;
            }
        }

        if($abort != [])
        {
            return response()->json(['warning' => 'skipped', 'message' => 'Could not delete users with these ids: ' . implode(', ', $abort)], 200);
        }
        return response()->json(null, 204);
    }

    public function updateUserAvatar(Request $request)
    {
        
        $this->validate($request, [
            'photo' => 'required|file|image|max:2048',
        ]);
        $user = User::find($request->id);
        
        if($user === null)
        {
            return response()->json(['error' => 'not_found'], 404);
        }

        abort_unless(Gate::allows('edit_user', auth()->user(), $user), 403, 'Gate');
        
        $newPath = $request->photo->store('users/profile', 'public');

        $user->photo_url = $newPath;

        if($user->save())
        {
            return response()->json(asset('storage/'.$newPath));
        }
        
        return response()->json(['error' => 'generic'], 401);
    }

    public function removeUserAvatar (Request $request)
    {
        
        $user = User::find($request->id);
        
        if($user === null)
        {
            return response()->json(['error' => 'not_found'], 404);
        }
        
        abort_unless(Gate::allows('edit_user', auth()->user(), $user), 403, 'Gate');
        
        $user->photo_url = null;

        if($user->save())
        {
            return response()->json(asset('images/profile/default-user.jpg'));
        }
        
        return response()->json(['error' => 'generic'], 401);
    }

    public function getCsvColumns(){
        $columns = [];
        $all = User::CSV_COLUMNS;
        foreach($all as $v)
        {
            $columns[$v] = false;
        }
        return response()->json($columns);
    }

    public function makeCsv(Request $request)
    {
        $this->validate($request, [
            'request_email' => 'email|required_if:to_email,==,1'
        ]);
        
        $selection_option = $request->data['selection_option'];
        $column_selection = $request->data['column_selection'];
        $csv_format = $request->data['csv_format'];
        $rows = $request->data['rows'];
        $columns = $this->filteredHeadings($request->data['columns']);
        if(count($columns) == 0 || $column_selection == 'all')
        {
            $columns = User::CSV_COLUMNS;
        }
        $builder = User::with('roles')->select($columns);
        if($selection_option == 1)
        {
            $builder->whereIn('id', collect($rows)->pluck('id')->toArray())->get();
        }
        $rows = $builder->get();
        $request_email = $request->data['request_email'];
        $to_email = $request->data['to_email'];
        $list_name = $request->data['list_name'];
        if($csv_format == 'excel')
        {
            $csv_format = 'xlsx';
        }
        $const_format = $this->getConstFormat($csv_format);
        $fileName = 'user_list_'.time().'.'.$csv_format;
        $export = new UserExport($to_email, $request_email, $csv_format, $rows->toArray(), $columns, $selection_option, $fileName);
        Excel::store($export, $fileName);
        $file = storage_path('app/'.$fileName);
        if($to_email)
        {
            $this->sendCsvMail($csv_format, count($rows), $request_email, $list_name, $file, $fileName);
            return response()->json(true);
        }
        
        $headers = $this->getContentTypeHeaderPrefix($csv_format, $fileName);

        return  response()->download($file, $fileName, $headers)->deleteFileAfterSend();
        
    }

    private function filteredHeadings($columns)
    {
        
        return collect($columns)->filter()->keys()->toArray();
    }

    private function sendCsvMail($csv_format, $user_count, $request_email, $list_name, $file, $fileName)
    {
        $subject = "Here is your {$csv_format} file";
        Mail::send('email.csv', ['csv_format' => $csv_format, 'no_records' => $user_count == 0 ? 'all' : $user_count, 'list_name' => $list_name],
            function($mail) use($subject, $file, $request_email, $fileName) {
                $mail->from(config('mail.from.address'), config('mail.from.name'));
                $mail->to($request_email);
                $mail->subject($subject);
                $mail->attach($file, ['as'=>$fileName]);
            });
    }

    private function getContentTypeHeaderPrefix($format, $filename)
    {
        switch($format){
            case 'csv': return [ 
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename=' . $filename,
                'Content-Transfer-Encoding' => 'binary',
                'Pragma' => 'public'
            ];
            case 'xlsx': return [ 
                'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'Content-Disposition' => 'attachment; filename=' . $filename,
                'Content-Transfer-Encoding' => 'binary',
                'Cache-Control' => 'must-revalidate',
                'Pragma' => 'public'
            ];
            case 'tsv': return [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename=' . $filename,
                'Content-Transfer-Encoding' => 'binary',
                'Pragma' => 'public'
            ];
            case 'ods': return [ 
                'Content-Type' => 'application/vnd.oasis.opendocument.spreadsheet',
                'Content-Disposition' => 'attachment; filename=' . $filename,
                'Content-Transfer-Encoding' => 'binary',
                'Cache-Control' => 'must-revalidate',
                'Pragma' => 'public'
            ];
            case 'xls': return [ 
                'Content-Type' => 'application/vnd.ms-excel',
                'Content-Disposition' => 'attachment; filename=' . $filename,
                'Content-Transfer-Encoding' => 'binary',
                'Cache-Control' => 'must-revalidate',
                'Pragma' => 'public'
            ];
            case 'html': return [ 
                'Content-Type' => 'text/html',
                'Content-Disposition' => 'attachment; filename=' . $filename,
                'Content-Transfer-Encoding' => 'binary',
                'Pragma' => 'public'
            ];
        }
    }

    private function getConstFormat($format)
    {
        //if(isset(config('excel.extension_detector')[$format]))
        //{
        //    return config('excel.extension_detector')[$format];
        //}
        //else
        //{
            switch($format){
                case 'csv': return \Maatwebsite\Excel\Excel::CSV;
                case 'xlsx': return \Maatwebsite\Excel\Excel::XLSX;
                case 'tsv': return \Maatwebsite\Excel\Excel::TSV;
                case 'ods': return \Maatwebsite\Excel\Excel::ODS;
                case 'xls': return \Maatwebsite\Excel\Excel::XLS;
                case 'html': return \Maatwebsite\Excel\Excel::HTML;
            }
        //}
    }

}
