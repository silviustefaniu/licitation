<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Menu;
use App\Page;
use App\Shortcode;
use Gate;

class PageController extends Controller
{
    public function index()
    {
        abort_unless(Gate::allows('list_page', auth()->user()), 403, 'Gate');
        $pages = Page::get();
        return response()->json($pages);
    }

    public function view(Request $request)
    {
        abort_unless(Gate::allows('edit_page', auth()->user()), 403, 'Gate');
        $page = Page::with(array('shortcodes' => function($query){
                return $query->orderBy('position')->whereNull('parent')->orWhere('parent', 0);
            }))->where('id', $request->id)->first();
        return response()->json($page);
    }

    public function store(Request $request)
    {
        abort_unless(Gate::allows('create_page', auth()->user()), 403, 'Gate');
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required'
        ]);
        $page = new Page();
        $page->name = $request->name;
        $page->url = $request->url;
        $page->metadata = $request->metadata;
        $page->visible = $request->visible;
        $page->save();
        foreach($request->shortcodes as $position => $shortcode)
        {
            $sh = new Shortcode();
            $sh->type = $shortcode['type'];
            $sh->parent = null;
            $sh->classes = $shortcode['classes'];
            $sh->content = json_encode($shortcode['content']);
            $sh->position = $position;
            $sh->page_id = $page->id;
            $sh->save();
            if(isset($shortcode['shortcodes']))
            {
                foreach($shortcode['shortcodes'] as $sub_position => $sub_shortcode)
                {
                    $sub_sh = new Shortcode();
                    $sub_sh->type = $sub_shortcode['type'];
                    $sub_sh->parent = $sh->id;
                    $sub_sh->classes = json_encode($sub_shortcode['classes']);
                    $sub_sh->content = json_encode($sub_shortcode['content']);
                    $sub_sh->position = $sub_position;
                    $sub_sh->page_id = $page->id;
                    $sub_sh->save();
                }
            }
        }

        return response()->json(['id' => $page->id]);
    }

    public function edit(Request $request)
    {
        abort_unless(Gate::allows('edit_page', auth()->user()), 403, 'Gate');
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required'
        ]);
        $page = Page::find($request->id);
        abort_if($page === null, 404);
        $page->name = $request->name;
        $page->url = $request->url;
        $page->metadata = $request->metadata;
        $page->visible = $request->visible;
        $page->save();
        $page->shortcodes()->delete();
        foreach($request->shortcodes as $position => $shortcode)
        {
        
                $sh = new Shortcode();
            
            $sh->type = $shortcode['type'];
            $sh->parent = null;
            $sh->classes = json_encode($shortcode['classes']);
            $sh->content = json_encode($shortcode['content']);
            $sh->position = $position;
            $sh->page_id = $page->id;
            $sh->save();
            if(isset($shortcode['shortcodes']))
            {
                foreach($shortcode['shortcodes'] as $sub_position => $sub_shortcode)
                {
                    $sub_sh = null;
                    if($sub_shortcode['id'] == 0)
                    {
                        $sub_sh = new Shortcode();
                    }
                    else
                    {
                        $sub_sh = Shortcode::find($sub_shortcode['id']);
                    }
                    if($sub_sh !== null)
                    {
                        $sub_sh->type = $sub_shortcode['type'];
                        $sub_sh->parent = $sh->id;
                        $sub_sh->classes = json_encode($sub_shortcode['classes']);
                        $sub_sh->content = json_encode($sub_shortcode['content']);
                        $sub_sh->position = $sub_position;
                        $sub_sh->page_id = $page->id;
                        $sub_sh->save();
                    }
                }
            }
        }

        return response()->json(true);
    }


    public function makePublic(Request $request)
    {
        abort_unless(Gate::allows('make_visible', auth()->user()), 403, 'Gate');
        $page = Page::find($request->id);
        $page->visible = $request->visible;
        $page->save();

        return response()->json(true);
    }


    public function delete(Request $request)
    {
        abort_unless(Gate::allows('delete_page', auth()->user()), 403, 'Gate');
        $page = Page::with('menus')->find($request->id);
        if($page->menus != null)
        {
            foreach($page->menus as $menu)
            {
                $menu->delete();
            }
        }
        $page->delete();
        return response()->json($page, 200);
    }
    
    public function uploadAdapter(Request $request)
    {
        abort_unless(Gate::allows('edit_page', auth()->user()), 403, 'Gate');

        $this->validate($request, [
            'upload' => 'required|file|image|max:2048',
        ]);
        if($newPath = $request->upload->storeAs('website/images', $request->file('upload')->getClientOriginalName(), 'public'))
        {
            return response()->json(['uploaded' => true, 'url' => asset('/storage/'.$newPath)]);
        }
        
        return response()->json(["uploaded" => false,
        "error" => [
            "message" => "could not upload this image"
        ]], 401);
    }
}
