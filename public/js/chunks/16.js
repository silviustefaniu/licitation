(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[16],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoleEditTabPermissions_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleEditTabPermissions.vue */ "./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue");
/* harmony import */ var _RoleEditTabInformation_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleEditTabInformation.vue */ "./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue");
/* harmony import */ var _store_role_moduleRole_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/store/role/moduleRole.js */ "./resources/js/src/store/role/moduleRole.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

 // Store Module


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    RoleEditTabPermissions: _RoleEditTabPermissions_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    RoleEditTabInformation: _RoleEditTabInformation_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      role_data: null,
      role_not_found: false,

      /*
        This property is created for fetching latest data from API when tab is changed
          Please check it's watcher
      */
      activeTab: 0
    };
  },
  watch: {
    activeTab: function activeTab() {
      this.fetch_role_data(this.$route.params.roleId);
    }
  },
  methods: {
    fetch_role_data: function fetch_role_data(roleId) {
      var _this = this;

      this.$store.dispatch('moduleRole/fetchRole', roleId).then(function (res) {
        _this.role_data = res.data;
      }).catch(function (err) {
        if (err.response.status === 404) {
          _this.role_not_found = true;
          return;
        }
      });
    }
  },
  created: function created() {
    if (!_store_role_moduleRole_js__WEBPACK_IMPORTED_MODULE_2__["default"].isRegistered) {
      this.$store.registerModule('moduleRole', _store_role_moduleRole_js__WEBPACK_IMPORTED_MODULE_2__["default"]);
      _store_role_moduleRole_js__WEBPACK_IMPORTED_MODULE_2__["default"].isRegistered = true;
    }

    this.fetch_role_data(this.$route.params.roleId);

    if (!this.activeUserInfo.permissions.includes('view_role')) {
      this.$vs.notify({
        title: 'Error',
        text: 'You don\'t have permission to view this role',
        iconPack: 'feather',
        icon: 'icon-alert-circle',
        color: 'danger'
      });
    }
  },
  computed: {
    activeUserInfo: function activeUserInfo() {
      return this.$store.state.AppActiveUser;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/core-js/json/stringify */ "./node_modules/@babel/runtime/core-js/json/stringify.js");
/* harmony import */ var _babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_1__);

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  props: {
    data: {
      type: Object,
      required: true
    }
  },
  data: function data() {
    return {
      data_local: JSON.parse(_babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default()(this.data))
    };
  },
  mounted: function mounted() {
    console.log(this.data, 'role');
  },
  computed: {
    validateForm: function validateForm() {
      return !this.errors.any();
    },
    activeUserInfo: function activeUserInfo() {
      return this.$store.state.AppActiveUser;
    }
  },
  methods: {
    capitalize: function capitalize(str) {
      return str.slice(0, 1).toUpperCase() + str.slice(1, str.length);
    },
    save_changes: function save_changes() {
      var _this = this;

      /* eslint-disable */
      if (!this.validateForm) return; // Loading

      this.$vs.loading();
      this.$store.dispatch('moduleRole/editRole', this.data_local).then(function () {
        _this.$vs.loading.close();
      }).catch(function (error) {
        _this.$vs.loading.close();

        _this.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });
      });
    },
    reset_data: function reset_data() {
      this.data_local = JSON.parse(_babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default()(this.data));
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/core-js/object/assign */ "./node_modules/@babel/runtime/core-js/object/assign.js");
/* harmony import */ var _babel_runtime_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/core-js/json/stringify */ "./node_modules/@babel/runtime/core-js/json/stringify.js");
/* harmony import */ var _babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_2__);


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  props: {
    data: {
      type: Object,
      required: true
    }
  },
  mounted: function mounted() {
    console.log(this.data_local);
  },
  data: function data() {
    return {
      data_local: JSON.parse(_babel_runtime_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_1___default()(this.data)),
      roles_modules: this.data.roles_modules,
      local_permissions: this.data.local_permissions
    };
  },
  computed: {
    validateForm: function validateForm() {
      return !this.errors.any();
    },
    activeUserInfo: function activeUserInfo() {
      return this.$store.state.AppActiveUser;
    }
  },
  methods: {
    save_changes: function save_changes() {
      var _this = this;

      /* eslint-disable */
      console.log(this.data_local);
      if (!this.validateForm) return; // Loading

      this.$vs.loading(); // Here will go your API call for updating data
      // You can get data in "this.data_local"

      this.$store.dispatch('moduleRole/editRolePermissions', {
        id: this.data.id,
        permissions: this.local_permissions
      }).then(function () {
        _this.$vs.loading.close();
      }).catch(function (error) {
        _this.$vs.loading.close();

        _this.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });
      });
      /* eslint-enable */
    },
    reset_data: function reset_data() {
      this.data_local = _babel_runtime_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default()({}, this.data);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=template&id=3fa4b3b4&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=template&id=3fa4b3b4& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-role-edit" } },
    [
      _c(
        "vs-alert",
        {
          attrs: {
            color: "danger",
            title: "Role Not Found",
            active: _vm.role_not_found
          },
          on: {
            "update:active": function($event) {
              _vm.role_not_found = $event
            }
          }
        },
        [
          _c("span", [
            _vm._v(
              "Role record with id: " +
                _vm._s(_vm.$route.params.roleId) +
                " not found. "
            )
          ]),
          _vm._v(" "),
          _c(
            "span",
            [
              _c("span", [_vm._v("Check ")]),
              _c(
                "router-link",
                {
                  staticClass: "text-inherit underline",
                  attrs: { to: { name: "app-roles-list" } }
                },
                [_vm._v("All Roles")]
              )
            ],
            1
          )
        ]
      ),
      _vm._v(" "),
      _vm.role_data
        ? _c("vx-card", [
            _c("div", { attrs: { slot: "header" }, slot: "header" }, [
              _c("h3", [
                _vm._v(
                  "\n        Role " + _vm._s(_vm.role_data.name) + "\n      "
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "tabs-container px-6 pt-6" },
              [
                _vm.activeUserInfo.permissions.includes("view_role") ||
                _vm.activeUserInfo.permissions.includes("edit_role")
                  ? _c(
                      "vs-tabs",
                      {
                        staticClass: "tab-action-btn-fill-conatiner",
                        model: {
                          value: _vm.activeTab,
                          callback: function($$v) {
                            _vm.activeTab = $$v
                          },
                          expression: "activeTab"
                        }
                      },
                      [
                        _c(
                          "vs-tab",
                          {
                            attrs: {
                              label: "Permissions",
                              "icon-pack": "feather",
                              icon: "icon-lock"
                            }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "tab-text" },
                              [
                                _c("role-edit-tab-permissions", {
                                  staticClass: "mt-4",
                                  attrs: { data: _vm.role_data }
                                })
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "vs-tab",
                          {
                            attrs: {
                              label: "Informati",
                              "icon-pack": "feather",
                              icon: "icon-info"
                            }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "tab-text" },
                              [
                                _c("role-edit-tab-information", {
                                  staticClass: "mt-4",
                                  attrs: { data: _vm.role_data }
                                })
                              ],
                              1
                            )
                          ]
                        )
                      ],
                      1
                    )
                  : _vm._e()
              ],
              1
            )
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=template&id=1ab389ab&":
/*!********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=template&id=1ab389ab& ***!
  \********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "role-edit-tab-info" } }, [
    _c("div", { staticClass: "vx-row" }, [
      _c(
        "div",
        { staticClass: "vx-col md:w-1/2 w-full" },
        [
          _c("vs-input", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required|alpha_spaces",
                expression: "'required|alpha_spaces'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: {
              label: "Name",
              readonly: !_vm.activeUserInfo.permissions.includes("edit_role"),
              name: "name"
            },
            model: {
              value: _vm.data_local.name,
              callback: function($$v) {
                _vm.$set(_vm.data_local, "name", $$v)
              },
              expression: "data_local.name"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("name"),
                  expression: "errors.has('name')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("name")))]
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "vx-row" }, [
      _c("div", { staticClass: "vx-col w-full" }, [
        _vm.activeUserInfo.permissions.includes("edit_role")
          ? _c(
              "div",
              { staticClass: "mt-8 flex flex-wrap items-center justify-end" },
              [
                _c(
                  "vs-button",
                  {
                    staticClass: "ml-auto mt-2",
                    attrs: { disabled: !_vm.validateForm },
                    on: { click: _vm.save_changes }
                  },
                  [_vm._v("Save Changes")]
                ),
                _vm._v(" "),
                _c(
                  "vs-button",
                  {
                    staticClass: "ml-4 mt-2",
                    attrs: { type: "border", color: "warning" },
                    on: { click: _vm.reset_data }
                  },
                  [_vm._v("Reset")]
                )
              ],
              1
            )
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=template&id=6d709d03&":
/*!********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=template&id=6d709d03& ***!
  \********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "role-edit-tab-info" } }, [
    _c("div", { staticClass: "vx-row" }, [
      _c(
        "div",
        { staticClass: "block overflow-x-auto" },
        _vm._l(_vm.roles_modules, function(permissions, module_name) {
          return _c("table", { key: module_name, staticClass: "w-full" }, [
            _c(
              "tr",
              [
                _c(
                  "th",
                  {
                    staticClass: "font-semibold text-base text-left px-3 py-2"
                  },
                  [_vm._v(_vm._s(module_name))]
                ),
                _vm._v(" "),
                _vm._l(permissions, function(name, index) {
                  return _c(
                    "td",
                    { key: index },
                    [
                      _c(
                        "vs-checkbox",
                        {
                          attrs: {
                            disabled: !_vm.activeUserInfo.permissions.includes(
                              "edit_role"
                            )
                          },
                          model: {
                            value: _vm.local_permissions[name],
                            callback: function($$v) {
                              _vm.$set(_vm.local_permissions, name, $$v)
                            },
                            expression: "local_permissions[name]"
                          }
                        },
                        [_vm._v(" " + _vm._s(name) + " ")]
                      )
                    ],
                    1
                  )
                })
              ],
              2
            )
          ])
        }),
        0
      )
    ]),
    _vm._v(" "),
    _vm.activeUserInfo.permissions.includes("edit_role")
      ? _c("div", { staticClass: "vx-row" }, [
          _c("div", { staticClass: "vx-col w-full" }, [
            _c(
              "div",
              { staticClass: "mt-8 flex flex-wrap items-center justify-end" },
              [
                _c(
                  "vs-button",
                  {
                    staticClass: "ml-auto mt-2",
                    attrs: { disabled: !_vm.validateForm },
                    on: { click: _vm.save_changes }
                  },
                  [_vm._v("Save Changes")]
                ),
                _vm._v(" "),
                _c(
                  "vs-button",
                  {
                    staticClass: "ml-4 mt-2",
                    attrs: { type: "border", color: "warning" },
                    on: { click: _vm.reset_data }
                  },
                  [_vm._v("Reset")]
                )
              ],
              1
            )
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/store/role/moduleRole.js":
/*!***************************************************!*\
  !*** ./resources/js/src/store/role/moduleRole.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _moduleRoleState_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moduleRoleState.js */ "./resources/js/src/store/role/moduleRoleState.js");
/* harmony import */ var _moduleRoleMutations_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./moduleRoleMutations.js */ "./resources/js/src/store/role/moduleRoleMutations.js");
/* harmony import */ var _moduleRoleActions_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./moduleRoleActions.js */ "./resources/js/src/store/role/moduleRoleActions.js");
/* harmony import */ var _moduleRoleGetters_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./moduleRoleGetters.js */ "./resources/js/src/store/role/moduleRoleGetters.js");
/*=========================================================================================
  File Name: moduleUserManagement.js
  Description: Calendar Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/




/* harmony default export */ __webpack_exports__["default"] = ({
  isRegistered: false,
  namespaced: true,
  state: _moduleRoleState_js__WEBPACK_IMPORTED_MODULE_0__["default"],
  mutations: _moduleRoleMutations_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  actions: _moduleRoleActions_js__WEBPACK_IMPORTED_MODULE_2__["default"],
  getters: _moduleRoleGetters_js__WEBPACK_IMPORTED_MODULE_3__["default"]
});

/***/ }),

/***/ "./resources/js/src/store/role/moduleRoleActions.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/store/role/moduleRoleActions.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/core-js/promise */ "./node_modules/@babel/runtime/core-js/promise.js");
/* harmony import */ var _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _axios_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/axios.js */ "./resources/js/src/axios.js");


/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

/* harmony default export */ __webpack_exports__["default"] = ({
  fetchRoles: function fetchRoles(_ref, item) {
    var commit = _ref.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/roles/index").then(function (response) {
        commit('SET_ROLES', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchRole: function fetchRole(_ref2, roleId) {
    var commit = _ref2.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/roles/".concat(roleId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  addRole: function addRole(_ref3, item) {
    var commit = _ref3.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/roles/store", {
        name: item.name
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editRole: function editRole(_ref4, item) {
    var commit = _ref4.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/roles/edit", {
        id: item.id,
        name: item.name
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeRole: function removeRole(_ref5, id) {
    var commit = _ref5.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/roles/delete", {
        id: id
      }).then(function (response) {
        commit('REMOVE_ROLE', id);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editRolePermissions: function editRolePermissions(_ref6, data) {
    var commit = _ref6.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/roles/edit-permissions", {
        id: data.id,
        permissions: data.permissions
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ "./resources/js/src/store/role/moduleRoleGetters.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/store/role/moduleRoleGetters.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./resources/js/src/store/role/moduleRoleMutations.js":
/*!************************************************************!*\
  !*** ./resources/js/src/store/role/moduleRoleMutations.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  SET_ROLES: function SET_ROLES(state, roles) {
    state.roles = roles;
  },
  REMOVE_ROLE: function REMOVE_ROLE(state, itemId) {
    var roleIndex = state.roles.findIndex(function (u) {
      return u.id === itemId;
    });
    state.roles.splice(roleIndex, 1);
  }
});

/***/ }),

/***/ "./resources/js/src/store/role/moduleRoleState.js":
/*!********************************************************!*\
  !*** ./resources/js/src/store/role/moduleRoleState.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarState.js
  Description: Calendar Module State
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  roles: []
});

/***/ }),

/***/ "./resources/js/src/views/apps/role/role-edit/RoleEdit.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/src/views/apps/role/role-edit/RoleEdit.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoleEdit_vue_vue_type_template_id_3fa4b3b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleEdit.vue?vue&type=template&id=3fa4b3b4& */ "./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=template&id=3fa4b3b4&");
/* harmony import */ var _RoleEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleEdit.vue?vue&type=script&lang=js& */ "./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleEdit_vue_vue_type_template_id_3fa4b3b4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RoleEdit_vue_vue_type_template_id_3fa4b3b4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/apps/role/role-edit/RoleEdit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleEdit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=template&id=3fa4b3b4&":
/*!************************************************************************************************!*\
  !*** ./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=template&id=3fa4b3b4& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEdit_vue_vue_type_template_id_3fa4b3b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleEdit.vue?vue&type=template&id=3fa4b3b4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEdit.vue?vue&type=template&id=3fa4b3b4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEdit_vue_vue_type_template_id_3fa4b3b4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEdit_vue_vue_type_template_id_3fa4b3b4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoleEditTabInformation_vue_vue_type_template_id_1ab389ab___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleEditTabInformation.vue?vue&type=template&id=1ab389ab& */ "./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=template&id=1ab389ab&");
/* harmony import */ var _RoleEditTabInformation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleEditTabInformation.vue?vue&type=script&lang=js& */ "./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleEditTabInformation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleEditTabInformation_vue_vue_type_template_id_1ab389ab___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RoleEditTabInformation_vue_vue_type_template_id_1ab389ab___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabInformation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleEditTabInformation.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabInformation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=template&id=1ab389ab&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=template&id=1ab389ab& ***!
  \**************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabInformation_vue_vue_type_template_id_1ab389ab___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleEditTabInformation.vue?vue&type=template&id=1ab389ab& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEditTabInformation.vue?vue&type=template&id=1ab389ab&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabInformation_vue_vue_type_template_id_1ab389ab___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabInformation_vue_vue_type_template_id_1ab389ab___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoleEditTabPermissions_vue_vue_type_template_id_6d709d03___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleEditTabPermissions.vue?vue&type=template&id=6d709d03& */ "./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=template&id=6d709d03&");
/* harmony import */ var _RoleEditTabPermissions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleEditTabPermissions.vue?vue&type=script&lang=js& */ "./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleEditTabPermissions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleEditTabPermissions_vue_vue_type_template_id_6d709d03___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RoleEditTabPermissions_vue_vue_type_template_id_6d709d03___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabPermissions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleEditTabPermissions.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabPermissions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=template&id=6d709d03&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=template&id=6d709d03& ***!
  \**************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabPermissions_vue_vue_type_template_id_6d709d03___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleEditTabPermissions.vue?vue&type=template&id=6d709d03& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/role/role-edit/RoleEditTabPermissions.vue?vue&type=template&id=6d709d03&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabPermissions_vue_vue_type_template_id_6d709d03___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditTabPermissions_vue_vue_type_template_id_6d709d03___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);