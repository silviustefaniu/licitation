(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[19],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store_cms_moduleCms_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/store/cms/moduleCms.js */ "./resources/js/src/store/cms/moduleCms.js");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuedraggable */ "./node_modules/vuedraggable/dist/vuedraggable.common.js");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vuedraggable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _nested_NestedMenus_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./nested/NestedMenus.vue */ "./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    draggable: vuedraggable__WEBPACK_IMPORTED_MODULE_1___default.a,
    NestedDraggable: _nested_NestedMenus_vue__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      drag_data: [],
      selectedMenu: {},
      popUpDeleteMenu: false,
      popUpVisibilityMenu: false,
      local_pages: [],
      local_menus_header: [],
      local_menus_footer: [],
      menu_name: "",
      menu_url: "",
      menu_blank: false
    };
  },
  watch: {},
  computed: {
    activeUserInfo: function activeUserInfo() {
      return this.$store.state.AppActiveUser;
    }
  },
  methods: {
    add_menu: function add_menu() {
      var _this = this;

      this.$vs.loading();
      var item = {
        name: this.menu_name,
        url: this.menu_url,
        blank: this.menu_blank
      };
      this.$store.dispatch('moduleCms/addMenu', item).then(function () {
        // Loading
        //this.$vs.loading.close()
        _this.$vs.notify({
          title: 'Success',
          text: "Menu added",
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'success'
        });

        _this.fetch_menus();
      }).catch(function (error) {
        _this.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });

        _this.$vs.loading.close();
      });
    },
    save_changes: function save_changes() {
      var _this2 = this;

      // Loading
      this.$vs.loading();
      var data = [];
      this.local_menus_header.forEach(function (element) {
        element.type = 'header';
        data.push(element);
      });
      this.local_menus_footer.forEach(function (element) {
        element.type = 'footer';
        data.push(element);
      });
      this.$store.dispatch('moduleCms/arrangeMenus', data).then(function () {
        // Loading
        _this2.$vs.loading.close();

        _this2.popUpVisibilityMenu = false;

        _this2.$vs.notify({
          title: 'Success',
          text: "Menus arrangament saved",
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'success'
        });

        _this2.fetch_menus();
      }).catch(function (error) {
        _this2.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });

        _this2.$vs.loading.close();
      });
    },
    resolve_menus: function resolve_menus() {
      if (typeof this.local_menus_header != "undefined" && this.local_menus_header.length > 0) {
        this.local_menus_header = this.local_menus_header.filter(function (x) {
          return x.type != "placeholder";
        });
        this.local_menus_header.forEach(function (element) {
          if (typeof element.siblings != "undefined" && element.siblings.length > 0) {
            element.siblings.forEach(function (sibling) {
              delete sibling.siblings;
            });
          }
        });
      } else {
        this.local_menus_header = [];
      }

      console.log(this.local_menus_header, "mh");

      if (typeof this.local_menus_footer != "undefined" && this.local_menus_footer.length > 0) {
        this.local_menus_footer = this.local_menus_footer.filter(function (x) {
          return x.type != "placeholder";
        });
        this.local_menus_footer.forEach(function (element) {
          if (typeof element.siblings != "undefined" && element.siblings.length > 0) {
            element.siblings.forEach(function (sibling) {
              delete sibling.siblings;
            });
          }
        });
      } else {
        this.local_menus_footer = [];
      }
    },
    fetch_menus: function fetch_menus() {
      var _this3 = this;

      this.$store.dispatch('moduleCms/fetchMenus').then(function () {
        _this3.local_menus_header = _this3.$store.state.moduleCms.menus.header;
        _this3.local_menus_footer = _this3.$store.state.moduleCms.menus.footer;
        console.log(_this3.local_menus_header);

        _this3.resolve_menus();

        _this3.$vs.loading.close();
      }).catch(function (error) {
        _this3.$vs.loading.close();

        _this3.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });
      });
    }
  },
  created: function created() {
    var _this4 = this;

    this.$vs.loading();

    if (!_store_cms_moduleCms_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered) {
      this.$store.registerModule('moduleCms', _store_cms_moduleCms_js__WEBPACK_IMPORTED_MODULE_0__["default"]);
      _store_cms_moduleCms_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered = true;
    }

    this.$store.dispatch('moduleCms/fetchPages').then(function () {
      _this4.local_pages = _this4.$store.state.moduleCms.pages;

      _this4.$vs.loading.close();
    }).catch(function (error) {
      console.log(error, 'funk');

      _this4.$vs.loading.close();

      _this4.$vs.notify({
        title: 'Error',
        text: error.response.data.message,
        iconPack: 'feather',
        icon: 'icon-alert-circle',
        color: 'danger'
      });
    });
    this.fetch_menus();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuedraggable */ "./node_modules/vuedraggable/dist/vuedraggable.common.js");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuedraggable__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    menus: {
      required: true,
      type: Array
    },
    type: ''
  },
  data: function data() {
    return {
      selectedMenu: {},
      popUpDeleteMenu: false,
      selectedIndex: 0,
      menuPlaceholder: [{
        id: 0,
        name: "Drag submenus here",
        url: null,
        class: "placeholder",
        page_id: 0
      }]
    };
  },
  components: {
    draggable: vuedraggable__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  name: "nested-draggable",
  methods: {
    deleteMenu: function deleteMenu(menu, confirmation, index) {
      var _this = this;

      this.selectedIndex = index;
      this.selectedMenu = menu;

      if (!confirmation) {
        this.popUpDeleteMenu = true;
      } else {
        this.$vs.loading();
        this.$store.dispatch('moduleCms/removeMenu', this.selectedMenu.id).then(function (response) {
          _this.$vs.loading.close();

          console.log(response);
          var data = response.data;
          var itemId = data.id;
          var parentId = data.parent;
          var type = data.type;

          if (typeof parentId == 'undefined' || parentId == null || parentId == 0) {
            var menuIndex = _this.menus.findIndex(function (u) {
              return u.id == itemId;
            });

            _this.menus.splice(menuIndex, 1);
          }

          _this.$vs.notify({
            title: 'Success',
            text: 'Menu deleted',
            iconPack: 'feather',
            icon: 'icon-alert-circle',
            color: 'success'
          });

          _this.popUpDeleteMenu = false;
        }).catch(function (error) {
          _this.popUpDeleteMenu = false;

          _this.$vs.loading.close();

          _this.$vs.notify({
            title: 'Error',
            text: error.response.data.message,
            iconPack: 'feather',
            icon: 'icon-alert-circle',
            color: 'danger'
          });
        });
      }
    },
    menuUrl: function menuUrl(menu) {
      console.log(menu);

      if (menu.url != null) {
        return menu.url;
      }

      if (menu.page != null) {
        return menu.page.url;
      }

      return "";
    }
  },
  computed: {
    activeUserInfo: function activeUserInfo() {
      return this.$store.state.AppActiveUser;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".placeholder {\n  font-size: 11px;\n}[dir] .placeholder {\n  padding: 2px;\n}\n[dir] .vs-list .vs-list {\n  background: #eaeaea;\n}\n[dir=ltr] .vs-list .vs-list {\n  margin-left: 12px;\n}\n[dir=rtl] .vs-list .vs-list {\n  margin-right: 12px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".simple {\n  color: #666;\n  font-style: italic;\n  font-weight: normal;\n}\n.link {\n  color: blueviolet;\n  font-style: italic;\n  font-weight: normal;\n}\n[dir] .vs-list .vs-list .main_draggable {\n  padding-bottom: 7px;\n}\n.vs-list .vs-list .main_draggable:after {\n  content: \"Drag submenu here\";\n  font-size: 11px;\n  font-weight: normal;\n  width: inherit;\n  position: relative;\n}\n[dir] .vs-list .vs-list .main_draggable:after {\n  background: transparent;\n  padding: 5px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./MenuList.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../../node_modules/css-loader!../../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NestedMenus.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=template&id=54715b49&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=template&id=54715b49& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "w-full", attrs: { id: "page-menu-list" } }, [
    _c(
      "div",
      { staticClass: "vs-col w-5/12" },
      [
        _c(
          "vx-card",
          [
            _c(
              "vs-list",
              [
                _c("vs-list-header", {
                  attrs: {
                    "icon-pack": "feather",
                    icon: "icon-plus",
                    title: "Create menu"
                  }
                }),
                _vm._v(" "),
                _c("vs-input", {
                  staticClass: "w-full",
                  attrs: { "label-placeholder": "Menu name" },
                  model: {
                    value: _vm.menu_name,
                    callback: function($$v) {
                      _vm.menu_name = $$v
                    },
                    expression: "menu_name"
                  }
                }),
                _vm._v(" "),
                _c("vs-input", {
                  staticClass: "w-full",
                  attrs: { "label-placeholder": "url" },
                  model: {
                    value: _vm.menu_url,
                    callback: function($$v) {
                      _vm.menu_url = $$v
                    },
                    expression: "menu_url"
                  }
                }),
                _vm._v(" "),
                _c(
                  "vs-checkbox",
                  {
                    model: {
                      value: _vm.menu_blank,
                      callback: function($$v) {
                        _vm.menu_blank = $$v
                      },
                      expression: "menu_blank"
                    }
                  },
                  [_vm._v("Open in new tab")]
                ),
                _vm._v(" "),
                _c(
                  "vs-button",
                  {
                    staticClass: "mt-2",
                    attrs: { color: "primary", type: "filled" },
                    on: {
                      click: function($event) {
                        return _vm.add_menu()
                      }
                    }
                  },
                  [_vm._v("Add")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("vs-divider"),
            _vm._v(" "),
            _c(
              "vs-list",
              [
                _c("vs-list-header", {
                  attrs: {
                    "icon-pack": "feather",
                    icon: "icon-grid",
                    title: "or add from Pages"
                  }
                }),
                _vm._v(" "),
                _c(
                  "draggable",
                  {
                    attrs: {
                      group: { name: "pages", pull: "clone", put: false },
                      list: _vm.local_pages
                    },
                    on: { end: _vm.resolve_menus }
                  },
                  _vm._l(_vm.local_pages, function(page, index) {
                    return _c(
                      "vs-list-item",
                      {
                        key: index,
                        staticClass: "item",
                        attrs: { title: page.name, subtitle: page.url }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "flex right" },
                          [
                            _vm.activeUserInfo.permissions.includes("edit_page")
                              ? _c("vs-button", {
                                  attrs: { color: "warning", icon: "create" },
                                  on: {
                                    click: function($event) {
                                      return _vm.$router.push(
                                        "/admin/cms/pages/edit/" + page.id
                                      )
                                    }
                                  }
                                })
                              : _vm._e()
                          ],
                          1
                        )
                      ]
                    )
                  }),
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "vs-col w-7/12" },
      [
        _c("vx-card", { attrs: { calss: "vs-row" } }, [
          _c(
            "div",
            { staticClass: "col" },
            [
              _c("nested-draggable", {
                attrs: { menus: _vm.local_menus_header, type: "header" }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col" },
            [
              _c("nested-draggable", {
                attrs: { menus: _vm.local_menus_footer, type: "footer" }
              })
            ],
            1
          )
        ]),
        _vm._v(" "),
        _vm.activeUserInfo.permissions.includes("manage_menus")
          ? _c(
              "div",
              { staticClass: "vx-row w-full" },
              [
                _c(
                  "vs-button",
                  {
                    staticClass: "ml-auto mt-2",
                    on: { click: _vm.save_changes }
                  },
                  [_vm._v("Save Changes")]
                )
              ],
              1
            )
          : _vm._e()
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=template&id=b259dade&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=template&id=b259dade& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "vs-list",
        [
          _vm.type
            ? _c("vs-list-header", {
                attrs: {
                  "icon-pack": "feather",
                  icon: "icon-grid",
                  title: _vm.type
                }
              })
            : _vm._e(),
          _vm._v(" "),
          _c(
            "draggable",
            {
              staticClass: "main_draggable",
              class: _vm.type,
              attrs: { group: { name: "pages" }, list: _vm.menus }
            },
            _vm._l(_vm.menus, function(menu, index) {
              return _c(
                "div",
                { key: index, staticClass: "menu-item", class: menu.class },
                [
                  _c(
                    "vs-list-item",
                    {
                      attrs: { title: menu.name, subtitle: _vm.menuUrl(menu) }
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "flex flex-end" },
                        [
                          _vm.activeUserInfo.permissions.includes(
                            "manage_menus"
                          ) && menu.id != 0
                            ? _c("vs-button", {
                                attrs: { color: "danger", icon: "delete" },
                                on: {
                                  click: function($event) {
                                    return _vm.deleteMenu(menu, false, index)
                                  }
                                }
                              })
                            : _vm._e()
                        ],
                        1
                      )
                    ]
                  ),
                  _vm._v(" "),
                  typeof menu.siblings != "undefined"
                    ? _c("nested-draggable", {
                        attrs: { menus: menu.siblings, type: false }
                      })
                    : _vm._e()
                ],
                1
              )
            }),
            0
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "vs-popup",
        {
          attrs: {
            title:
              "Delete menu " +
              "" +
              _vm.selectedMenu.name +
              " and all of it's contents?",
            active: _vm.popUpDeleteMenu
          },
          on: {
            "update:active": function($event) {
              _vm.popUpDeleteMenu = $event
            }
          }
        },
        [
          _c("p", [
            _vm._v(
              "\n      Are you sure you want to delete " +
                _vm._s(_vm.selectedMenu.name) +
                "?\n    "
            )
          ]),
          _vm._v(" "),
          _c(
            "vs-button",
            {
              attrs: { color: "danger", type: "filled" },
              on: {
                click: function($event) {
                  return _vm.deleteMenu(_vm.selectedMenu, true, 0)
                }
              }
            },
            [_vm._v("Delete")]
          ),
          _vm._v(" "),
          _c(
            "vs-button",
            {
              attrs: { color: "dark", type: "border" },
              on: {
                click: function($event) {
                  _vm.popUpDeleteMenu = !_vm.popUpDeleteMenu
                }
              }
            },
            [_vm._v("Cancel")]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/apps/cms/menu-list/MenuList.vue":
/*!****************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/menu-list/MenuList.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MenuList_vue_vue_type_template_id_54715b49___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MenuList.vue?vue&type=template&id=54715b49& */ "./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=template&id=54715b49&");
/* harmony import */ var _MenuList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MenuList.vue?vue&type=script&lang=js& */ "./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _MenuList_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MenuList.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _MenuList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MenuList_vue_vue_type_template_id_54715b49___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MenuList_vue_vue_type_template_id_54715b49___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/apps/cms/menu-list/MenuList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./MenuList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./MenuList.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=template&id=54715b49&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=template&id=54715b49& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_template_id_54715b49___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./MenuList.vue?vue&type=template&id=54715b49& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/MenuList.vue?vue&type=template&id=54715b49&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_template_id_54715b49___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuList_vue_vue_type_template_id_54715b49___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue":
/*!**************************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NestedMenus_vue_vue_type_template_id_b259dade___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NestedMenus.vue?vue&type=template&id=b259dade& */ "./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=template&id=b259dade&");
/* harmony import */ var _NestedMenus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NestedMenus.vue?vue&type=script&lang=js& */ "./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _NestedMenus_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./NestedMenus.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _NestedMenus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NestedMenus_vue_vue_type_template_id_b259dade___WEBPACK_IMPORTED_MODULE_0__["render"],
  _NestedMenus_vue_vue_type_template_id_b259dade___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NestedMenus.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/style-loader!../../../../../../../../node_modules/css-loader!../../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NestedMenus.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=template&id=b259dade&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=template&id=b259dade& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_template_id_b259dade___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NestedMenus.vue?vue&type=template&id=b259dade& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/menu-list/nested/NestedMenus.vue?vue&type=template&id=b259dade&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_template_id_b259dade___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NestedMenus_vue_vue_type_template_id_b259dade___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);