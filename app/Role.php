<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    public $timestamps = false;
    protected $casts = [
        'permissions' => 'array',
    ];

    protected $hidden = [];

    protected $appends = ['local_permissions', 'roles_modules'];

    const PERMISSIONS = [
        'users' => [
            'list_user',
            'view_user',
            'create_user',
            'edit_user',
            'delete_user',
        ],
        'roles' => [
            'list_role',
            'view_role',
            'create_role',
            'edit_role',
            'delete_role',
        ],
        'cms' => [
            'list_page',
            'create_page',
            'edit_page',
            'delete_page',
            'visible_page',
            'manage_menus',
        ]
    ];

    const ADMIN = 4;
    const SUPERADMIN = 3;
    const USER = 1;

    const ADMINS = [3,4];

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_roles', 'role_id', 'user_id');
    }

    public function hasPermission($permission)
    {
        if($this->permissions === null || $this->permissions == [])
        {
            return false;
        }
        return in_array($permission, $this->permissions);
    }

    public function getRolesModulesAttribute()
    {
        $_permissions = self::PERMISSIONS;
        $admin_permissions = self::find(self::ADMIN)->permissions;
        $return = [];
        foreach($_permissions as $module => $permissions)
        {
            foreach($permissions as $key => $perm)
            {
                if(in_array($perm, $admin_permissions))
                {
                    $return[$module][$key] = $perm;
                }
            }
        }
        return $return;
    }

    public function isValid()
    {
        if($this->id == self::ADMIN || $this->id == self::SUPERADMIN || $this->id == self::USER)
        {
            return false;
        }
        return true;
    }

    public function getLocalPermissionsAttribute()
    {
        $_prs = collect(self::PERMISSIONS)->collapse()->toArray();
        $permissions = [];
        foreach($_prs as $p)
        {
            if($this->hasPermission($p))
            {
                $permissions[$p] = true;
            }
            else
            {
                $permissions[$p] = false;
            }
        }
        return $permissions;
    }
    static public function getPermissionArray($role, $permission)
    {
        return array_push($role->permissions, $permission);
    }

    static public function dropPermissionArray($role, $permission)
    {
        return array_diff($role->permissions, [$permission]);
    }

    static public function checkPermissionsExists($permissions)
    {
        $_permissions = collect(self::PERMISSIONS)->collapse()->toArray();
        return !array_diff($permissions, $_permissions);
    }

    static public function filterPermissions($_prs)
    {
        $data = [];
        $permissions = collect(self::PERMISSIONS)->collapse()->toArray();
        foreach($_prs as $key => $p)
        {
            if($p)
            {
                $data[] = $key;
            }
        }
        return $data;
    }
}

?>