<?php

namespace App\Exceptions;

Use Throwable;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    //public function report(Exception$exception);
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     * @return \Illuminate\Http\Response
     */
    //public function render($request, Exception $exception);
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof HttpException) {
            if($exception->getMessage() == 'Unauthorized')
            {
                return response()->json([
                    'message' => 'Looks like your session has expired. Please Login again.'
                    , 'error' => 'Token'
                ], 401);
            }
            if($exception->getMessage() == 'Gate')
            {
                return response()->json([
                    'message' => 'You don\' have access to this request',
                    'error' => 'Gate'
                ], 401);
            }
            if($exception->getMessage() == 'already_exists')
            {
                return response()->json([
                    'message' => 'Resource already exists',
                    'error' => 'Resource'
                ], 401);
            }
        }
        return parent::render($request, $exception);
    }
}
