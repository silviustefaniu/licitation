<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        \Response::macro('attachment', function ($content, $filename, $content_type) {

            $headers = [
                'Content-type'        => $content_type,
                'Content-Disposition' => 'attachment; filename="'.$filename.'"',
            ];
        
            return \Response::make($content, 200, $headers);
        
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
