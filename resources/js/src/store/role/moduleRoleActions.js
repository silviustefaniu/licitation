/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {
  fetchRoles({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios.get("/api/roles/index")
        .then((response) => {
          commit('SET_ROLES', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchRole({ commit }, roleId) {
    return new Promise((resolve, reject) => {
      axios.get(`/api/roles/${roleId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  addRole({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios.post("/api/roles/store", {name: item.name})
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editRole({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios.post("/api/roles/edit", {id: item.id, name: item.name})
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  removeRole({ commit }, id) {
    return new Promise((resolve, reject) => {
      axios.post("/api/roles/delete", {id: id})
        .then((response) => {
          commit('REMOVE_ROLE', id)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editRolePermissions({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post("/api/roles/edit-permissions", {id: data.id, permissions: data.permissions})
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
}
