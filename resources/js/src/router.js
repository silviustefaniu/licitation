import Vue from 'vue'
import Router from 'vue-router'
import auth from '@/auth/authService'


Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: '/',
  scrollBehavior () {
    return { x: 0, y: 0 }
  },
  routes: [
    // =============================================================================
    // USER DEFINED PAGES
    // =============================================================================
    {
      path: '/login/:code?',
      component: () => import('./front/login/Login.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/forgot-password',
      component: () => import('./front/login/ForgotPassword.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/reset-password/:uuid?',
      component: () => import('./front/login/ResetPassword.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/logout/:code?',
      redirect: '/login/:code?',
      meta: {rule: 'public'}
    },
    {
      path: '/register',
      component: () => import('./front/register/Register.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/verify/:code?',
      redirect: '/login/:code?',
      meta: {rule: 'public'}
    },
    {
      path: '/phone',
      component: () => import('./front/login/AskPhoneNo.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/first-login-personal',
      component: () => import('./front/login/FirstLoginPersonal.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/first-login-firma',
      component: () => import('./front/login/FirstLoginCompany.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/first-login',
      component: () => import('./front/login/FirstLogin.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/index-user-felicitari',
      component: () => import('./front/login/FirstLoginGz.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/index-user-zero',
      component: () => import('./front/login/LoginUserZero.vue'),
      meta: {rule: 'public'}
    },
    {
      path: '/first-login-delete',
      component: () => import('./front/login/FirstLoginDelete.vue'),
      meta: {rule: 'public'}
    },
    
    // =============================================================================
    // ADMIN LAYOUT ROUTES
    // =============================================================================
    {
      path: '/admin',
      component: () => import('@/layouts/main/Main.vue'),
      
      children: [
        {
          path: '/admin/dashboard',
          redirect: '/admin/dashboard/analytics',
          meta: { rule: 'manager' },
        },
        {
          path: '/admin/dashboard/analytics',
          name: 'dashboard-analytics',
          component: () => import('@/views/DashboardAnalytics.vue'),
          meta: {
            pageTitle: 'Dashboard Analytics',
            rule: 'manager'
          }
        },
        {
          path: '/admin/dashboard/ecommerce',
          name: 'dashboard-ecommerce',
          component: () => import('@/views/DashboardECommerce.vue'),
          meta: {
            pageTitle: 'Dashboard Ecommerce',
            rule: 'manager'
          }
        },
        {
          path: '/admin/user/user-list',
          name: 'app-user-list',
          component: () => import('@/views/apps/user/user-list/UserList.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'Users', active: true }
            ],
            pageTitle: 'User List',
            rule: 'manager'
          }
        },
        {
          path: '/admin/user/create',
          name: 'app-user-list',
          component: () => import('@/views/apps/user/user-edit/UserNew.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'Users', url: '/admin/user/user-list' },
              { title: 'New', active: true }
            ],
            pageTitle: 'User Create',
            rule: 'manager'
          }
        },
        {
          path: '/admin/user/user-edit/:userId',
          name: 'app-user-edit',
          component: () => import('@/views/apps/user/user-edit/UserEdit.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'Users', url: '/admin/user/user-list' },
              { title: 'Edit', active: true }
            ],
            pageTitle: 'User Edit',
            rule: 'manager',
          }
        },
        {
          path: '/admin/user/roles',
          name: 'app-roles-list',
          component: () => import('@/views/apps/role/role-list/RoleList.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'Roles', active: true }
            ],
            pageTitle: 'Roles',
            rule: 'manager'
          }
        },
        {
          path: '/admin/user/roles/new',
          name: 'app-roles-list',
          component: () => import('@/views/apps/role/RoleView.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'Roles', url: '/admin/user/roles' },
              { title: 'Role new', active: true }
            ],
            pageTitle: 'New Role',
            rule: 'manager'
          }
        },
        {
          path: '/admin/user/roles/edit/:roleId',
          name: 'app-roles-edit',
          component: () => import('@/views/apps/role/role-edit/RoleEdit.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'Roles', url: '/admin/user/roles' },
              { title: 'Role Edit', active: true }
            ],
            pageTitle: 'Roles Edit',
            rule: 'manager'
          }
        },

        {
          path: '/admin/cms/menus',
          name: 'app-cms-pages',
          component: () => import('@/views/apps/cms/menu-list/MenuList.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'Website Menus', active: true }
            ],
            pageTitle: 'Website CMS Menus Management',
            rule: 'manager'
          }
        },
        {
          path: '/admin/cms/pages',
          name: 'app-cms-pages',
          component: () => import('@/views/apps/cms/page-list/PageList.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'CMS', active: true }
            ],
            pageTitle: 'CMS Pages',
            rule: 'manager'
          }
        },
        {
          path: '/admin/cms/pages/create',
          name: 'app-cms-page-create',
          component: () => import('@/views/apps/cms/PageNew.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'CMS', url: '/admin/cms/pages' },
              { title: 'New Page', active: true }
            ],
            rule: 'manager'
          }
        },
        {
          path: '/admin/cms/pages/edit/:pageId',
          name: 'app-cms-page-edit',
          component: () => import('@/views/apps/cms/PageEdit.vue'),
          meta: {
            breadcrumb: [
              { title: 'Home', url: '/admin/dashboard' },
              { title: 'CMS', url: '/admin/cms/pages' },
              { title: 'Edit Page', active: true }
            ],
            pageTitle: 'CMS Menus',
            rule: 'manager'
          }
        }
      ]
    },
    {
      path: '/',
      meta: {rule: 'public'},
      component: () => import('./front/Main.vue'),
    },
    {
      path: '/:url?',
      component: () => import('./front/Main.vue'),
      meta: {rule: 'public'},
      children: [
        {
          path: '/profile',
          component: () => import('@/front/profile/ProfileEdit.vue'),
          meta: {rule: 'user'}
        },
        {
          path: '/not-authorized',
          meta: {rule: 'public'},
          component: () => import('@/views/pages/NotAuthorized.vue')
        },
        {
          path: '/error-404',
          name: 'error-404',
          meta: {rule: 'public'},
          component: () => import('@/views/pages/Error404.vue')
        },
        {
          path: '/admin',
          name: 'admin-login',
          meta: {rule: 'public'},
          redirect: '/login',
        },

      ]
    },
    
    /*{
      path: '*',
      meta: {rule: 'public'},
      redirect: '/error-404'
    }*/

  ]
})

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
  if (appLoading) {
    appLoading.style.display = 'none'
  }
})

export default router
