import axios from '../../http/axios/index.js'

export default {
  SET_BEARER (state, access_token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`
  }
}
