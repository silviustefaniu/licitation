(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[27],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/AskPhoneNo.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/AskPhoneNo.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      phone: '',
      isTermsConditionAccepted: true,
      verify_form: false,
      sms_code: ''
    };
  },
  computed: {
    activeUserInfo: function activeUserInfo() {
      return this.$store.state.AppActiveUser;
    }
  },
  methods: {
    send_sms: function send_sms() {
      var _this = this;

      this.$vs.loading();
      var data = {
        phone: this.phone
      };
      this.$store.dispatch('auth/sendSmsCode', data).then(function (response) {
        _this.$vs.loading.close();

        _this.$vs.notify({
          title: 'Sms sent to ' + _this.phone,
          text: "Check your messages and follow the instructions to activate your account",
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'success'
        });

        _this.verify_form = true;
      }).catch(function (error) {
        _this.$vs.loading.close();

        _this.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });
      });
    },
    verify_phone: function verify_phone() {
      var _this2 = this;

      this.$vs.loading();
      var data = {
        sms_code: this.sms_code
      };
      this.$store.dispatch('auth/verifyPhone', data).then(function (response) {
        _this2.$vs.loading.close();

        _this2.$vs.notify({
          title: 'Your phone has been verified',
          text: "Continue to your account . . .",
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'success'
        });

        if (_this2.activeUserInfo.first_login == 1) {
          _this2.$router.push('/first-login');
        } else {
          _this2.$router.push('/');
        }
      }).catch(function (error) {
        _this2.$vs.loading.close();

        _this2.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/AskPhoneNo.vue?vue&type=template&id=3d6627cb&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/AskPhoneNo.vue?vue&type=template&id=3d6627cb& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "h-screen flex w-full bg-img" }, [
    _c(
      "div",
      { staticClass: "vx-col w-10/12 m-4 p-2 ml-auto mr-auto pagina_simpla" },
      [
        !_vm.verify_form
          ? _c("div", [
              _c("h1", [_vm._v("BUN VENIT!")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "\n              Phasellus vestibulum, nisl iaculis venenatis sodales, eros mi vulputate elit, eget pretium urna libero et dui. \n              Suspendisse sit amet velit malesuada enim porta fermentum. Maecenas velit felis, tristique vel massa non, \n              facilisis suscipit arcu. Etiam vel aliquam justo. Phasellus hendrerit ex eu ante consequat dictum. \n              Nullam ligula metus, lobortis sed tristique sed, pellentesque et quam. Nullam sed arcu nec quam mattis sagittis nec at mi. \n              Vestibulum placerat nunc in scelerisque porttitor. Mauris condimentum magna dapibus semper varius. Quisque at porttitor nulla. \n              Aliquam erat volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. \n              Quisque quis vulputate velit, pellentesque sodales massa. Phasellus ac neque at ex vestibulum tristique eleifend non velit. \n              Vivamus eget pellentesque odio.\n          "
                )
              ])
            ])
          : _c("div", [
              _c("h1", [_vm._v("VALIDEAZA NUMARUL DE TELEFON")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "\n              Phasellus vestibulum, nisl iaculis venenatis sodales, eros mi vulputate elit, eget pretium urna libero et dui. \n              Suspendisse sit amet velit malesuada enim porta fermentum. Maecenas velit felis, tristique vel massa non, \n              facilisis suscipit arcu. Etiam vel aliquam justo. Phasellus hendrerit ex eu ante consequat dictum. \n              Nullam ligula metus, lobortis sed tristique sed, pellentesque et quam. Nullam sed arcu nec quam mattis sagittis nec at mi. \n              Vestibulum placerat nunc in scelerisque porttitor. Mauris condimentum magna dapibus semper varius. Quisque at porttitor nulla. \n              Aliquam erat volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. \n              Quisque quis vulputate velit, pellentesque sodales massa. Phasellus ac neque at ex vestibulum tristique eleifend non velit. \n              Vivamus eget pellentesque odio.\n          "
                )
              ])
            ]),
        _vm._v(" "),
        !_vm.verify_form
          ? _c("div", { staticClass: "contact-formular" }, [
              _c(
                "div",
                { staticClass: "formular xl:w-8/12 w-10/12 ml-auto mr-auto" },
                [
                  _c("vs-input", {
                    staticClass: "w-full formular-entry",
                    attrs: { type: "text", label: "NUMAR TELEFON" },
                    model: {
                      value: _vm.phone,
                      callback: function($$v) {
                        _vm.phone = $$v
                      },
                      expression: "phone"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "centerx labelx formular-entry-box formular-entry"
                    },
                    [
                      _c("label", [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.isTermsConditionAccepted,
                              expression: "isTermsConditionAccepted"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(_vm.isTermsConditionAccepted)
                              ? _vm._i(_vm.isTermsConditionAccepted, null) > -1
                              : _vm.isTermsConditionAccepted
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.isTermsConditionAccepted,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    (_vm.isTermsConditionAccepted = $$a.concat([
                                      $$v
                                    ]))
                                } else {
                                  $$i > -1 &&
                                    (_vm.isTermsConditionAccepted = $$a
                                      .slice(0, $$i)
                                      .concat($$a.slice($$i + 1)))
                                }
                              } else {
                                _vm.isTermsConditionAccepted = $$c
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("span", [
                          _vm._v(
                            "\n                        Sunt de acord cu bla bla si pentru ca am dat bifat aici pot apasa si butonul de trimite. \n                        Sunt de acord cu bla bla si pentru ca am dat bifat aici pot apasa si butonul de trimite.\n                        "
                          )
                        ])
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "clearfix" }),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "my-4 w-full btn-executa",
                      on: { click: _vm.send_sms }
                    },
                    [_vm._v("trimite-mi cod sms")]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "formular-help" },
                    [
                      _c("router-link", { attrs: { to: "/faq" } }, [
                        _vm._v("(nu te descurci - vezi indrumari)")
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ])
          : _c("div", { staticClass: "contact-formular" }, [
              _c(
                "div",
                {
                  staticClass: "formular lg:w-8/12 md:w-10/12 ml-auto mr-auto"
                },
                [
                  _c("vs-input", {
                    staticClass: "w-full formular-entry",
                    attrs: { type: "text", label: "COD SMS" },
                    model: {
                      value: _vm.sms_code,
                      callback: function($$v) {
                        _vm.sms_code = $$v
                      },
                      expression: "sms_code"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "my-4 w-full btn-executa",
                      on: { click: _vm.verify_phone }
                    },
                    [_vm._v("valideaza")]
                  ),
                  _vm._v(" "),
                  _c("input", {
                    staticClass: "hidden",
                    attrs: { type: "submit" }
                  }),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "formular-help" },
                    [
                      _c("router-link", { attrs: { to: "/faq" } }, [
                        _vm._v("(nu te descurci - vezi indrumari)")
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ])
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/front/login/AskPhoneNo.vue":
/*!*****************************************************!*\
  !*** ./resources/js/src/front/login/AskPhoneNo.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AskPhoneNo_vue_vue_type_template_id_3d6627cb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AskPhoneNo.vue?vue&type=template&id=3d6627cb& */ "./resources/js/src/front/login/AskPhoneNo.vue?vue&type=template&id=3d6627cb&");
/* harmony import */ var _AskPhoneNo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AskPhoneNo.vue?vue&type=script&lang=js& */ "./resources/js/src/front/login/AskPhoneNo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AskPhoneNo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AskPhoneNo_vue_vue_type_template_id_3d6627cb___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AskPhoneNo_vue_vue_type_template_id_3d6627cb___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/front/login/AskPhoneNo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/front/login/AskPhoneNo.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/src/front/login/AskPhoneNo.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AskPhoneNo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AskPhoneNo.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/AskPhoneNo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AskPhoneNo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/front/login/AskPhoneNo.vue?vue&type=template&id=3d6627cb&":
/*!************************************************************************************!*\
  !*** ./resources/js/src/front/login/AskPhoneNo.vue?vue&type=template&id=3d6627cb& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AskPhoneNo_vue_vue_type_template_id_3d6627cb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AskPhoneNo.vue?vue&type=template&id=3d6627cb& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/AskPhoneNo.vue?vue&type=template&id=3d6627cb&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AskPhoneNo_vue_vue_type_template_id_3d6627cb___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AskPhoneNo_vue_vue_type_template_id_3d6627cb___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);