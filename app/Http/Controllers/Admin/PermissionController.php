<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles',
        ]);

        abort_unless(Gate::allows('add_permission', auth()->user()), 403, 'Gate');
        
        $role = Role::find($request->id);
        if($role->save())
        {
            return response()->json($role->name);
        }
        return response()->json(['error' => 'generic'], 401);
    }

}
