<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;

class IsManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if($user === null)
        {
            return middlewareError();
        }
        
        if (in_array($user->current_role, Role::ADMINS)) 
        {
            return $next($request);
        }

        $permissions_data = [];
        foreach($user->roles as $role)
        {
            $permissions_data[] = collect($role->local_permissions)->filter()->toArray();
        }

        foreach($permissions_data as $v)
        {
            if(count($v) > 0)
            {
                return $next($request);
            }
        }

        return middlewareError();
    }
}
