<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api' ], function () {
        Route::group([
                'namespace' => 'Auth',
                'prefix' => 'auth',
            ], function() {
                Route::post('login', 'AuthController@login');
                Route::post('forgot-password', 'ForgotPasswordController@forgotPassword');
                Route::get('reset-password/{uuid}', 'ForgotPasswordController@verifyResetLink');
                Route::post('reset-password', 'ForgotPasswordController@resetPassword');
                Route::post('logout', 'AuthController@logout');
                Route::post('refresh', 'AuthController@refresh');
                Route::get('user', 'AuthController@user');
                Route::get('user/{token}', 'AuthController@fetchUser');
                
                Route::post('registerUser', 'RegisterController@registerUser');
                Route::post('verify', 'RegisterController@verify');
                Route::post('resend', 'RegisterController@resendEmail')->middleware(['jwt']);
                Route::post('send-sms', 'RegisterController@sendSms');
                Route::post('verify-phone', 'RegisterController@verifyPhone')->middleware(['jwt', 'verified.email']);
            }
        );
        Route::group([
                'middleware' => ['jwt', 'verified.email', 'verified.phone'],
                'namespace' => 'Resources',
            ], 
            function () {
                Route::post('first-login-personal-data', 'UserController@firstLoginPersonalData');
                Route::post('first-login-company-data', 'UserController@firstLoginCompanyData');
                Route::post('profile', 'UserController@updateProfile');
                Route::post('profile/account', 'UserController@updateProfileAccount');
                Route::post('profile/avatar', 'UserController@updateProfileAvatar');
                Route::post('profile/remove-avatar', 'UserController@removeProfileAvatar');
            }
        );
        Route::group([
                'middleware' => ['jwt', 'verified.email'],
                'namespace' => 'Resources',
            ], 
            function () {
                Route::post('first-login-refuse', 'UserController@firstLoginRefuse');
            }
        );
        Route::group([
                'middleware' => ['jwt', 'manager'],
                'namespace' => 'Admin'
            ], 
            function () {
                Route::post('make-csv', 'AdminController@makeCsv');
                Route::get('get-csv-columns', 'AdminController@getCsvColumns');
                Route::get('users/index','AdminController@indexUsers');
                Route::get('users/{id}','AdminController@showFormUser');
                Route::post('users/create', 'AdminController@store');
                Route::post('users/update-info', 'AdminController@updateUser');
                Route::post('users/avatar','AdminController@updateUserAvatar');
                Route::post('users/remove-avatar','AdminController@removeUserAvatar');
                Route::delete('users/{id}','AdminController@deleteUser');
                Route::post('users/delete-bulk', 'AdminController@deleteBulk');

                Route::get('roles/index', 'RoleController@index');
                Route::get('roles/{id}', 'RoleController@view');
                Route::post('roles/store', 'RoleController@store');
                Route::post('roles/edit', 'RoleController@edit');
                Route::post('roles/delete', 'RoleController@delete');
                Route::post('roles/edit-permissions', 'RoleController@editPermissions');
                Route::post('roles/add-permission', 'RoleController@storePermission');
                Route::post('roles/edit-permission', 'RoleController@editPermission');
                Route::delete('roles/delete-permission', 'RoleController@deletePermission');

                Route::get('pages/index', 'PageController@index');
                Route::get('pages/{id}', 'PageController@view');
                Route::post('pages/make-visible', 'PageController@makePublic');
                Route::post('pages/create', 'PageController@store');
                Route::post('pages/edit', 'PageController@edit');
                Route::post('pages/delete', 'PageController@delete');
                Route::post('pages/uploadadapter', 'PageController@uploadAdapter');

                Route::get('menus/index', 'MenuController@index');
                Route::get('menus/{id}', 'MenuController@view');
                Route::post('menus/create', 'MenuController@store');
                Route::post('menus/edit', 'MenuController@edit');
                Route::post('menus/delete', 'MenuController@delete');
                Route::post('menus/arrange', 'MenuController@arrange');
                Route::post('menus/link-menu', 'MenuController@linkMenu');
                
                Route::post('shortcodes/create', 'ShortcodeController@store');
                Route::post('shortcodes/upload', 'ShortcodeController@upload');
                Route::post('shortcodes/edit', 'ShortcodeController@edit');
                Route::delete('shortcodes/{id}', 'ShortcodeController@delete');
                Route::post('shortcodes/arrange', 'ShortcodeController@arrange');
            }
        );
    }
);

/** CMS */
Route::group([
    'middleware' => 'api',
], function () {
    Route::get('home/menus', 'HomeController@getMenus');
    Route::get('home/pages/{url}', 'HomeController@getPageContent');
});