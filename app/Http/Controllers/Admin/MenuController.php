<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Menu;
use App\Page;
use App\Shortcode;

class MenuController extends Controller
{
    public function index()
    {
        $menus = Menu::whereNull('parent')->orWhere('parent', 0)->orderBy('position')->get()->groupBy('type');
        return response()->json($menus);
    }

    public function view(Request $request)
    {
        $menu = Menu::with(array('shortcodes' => function($query){
                return $query->orderBy('position');
            }))->where('id', $request->id)->first();
        
        return response()->json($menu);
    }

    public function store(Request $request)
    {
        $menu = new Menu();
        if($request->url === null)
        {
            $menu->class = "simple";
        }
        else
        {
            $menu->class = "link";
        }
        $menu->name = $request->name;
        $menu->url = $request->url;
        $menu->target_type = $request->blank;
        $menu->position = 0;
        $menu->type = "header";
        $menu->save();

        return response()->json($menu);
    }

    public function edit(Request $request)
    {
        $menu = Menu::find($request->id);
        $menu->name = $request->name;
        $menu->save();

        return response()->json(true);
    }

    public function linkMenu(Request $request)
    {
        $page = Page::find($request->id);
        abort_if($page === null, 404);
        
        $menu = new Menu();
        $menu->name = $page->name;
        $menu->position = $request->position;
        $menu->type = $request->type;
        $menu->save();

        return response()->json(true);
    }

    public function delete(Request $request)
    {
        $menu = Menu::find($request->id);
        $id = $menu->id;
        $menu->delete();
        unset($menu->page);
        return response()->json($menu);
    }


    public function arrange(Request $request)
    {
        $data = $request->data;
        foreach($data as $position => $menu )
        {
            if($menu['id'] != 0)
            {
                $_menu = Menu::find($menu['id']);
                if($_menu === null)
                {
                    $_menu = new Menu();
                    $_menu->name = $menu['name'];
                    $_menu->page_id = $menu['id'];
                }
                $_menu->position = $position;
                $_menu->parent = null;
                $_menu->type = $menu['type'];
                $_menu->save();

                if(isset($menu['siblings']) && count($menu['siblings']))
                {
                    foreach($menu['siblings'] as $sub_position => $submenu)
                    {
                        if($submenu['id'] != 0)
                        {
                            $_submenu = Menu::find($submenu['id']);
                            if($_submenu === null)
                            {
                                $_submenu = new Menu();
                                $_submenu->name = $submenu['name'];
                                $_submenu->page_id = $submenu['id'];
                            }
                            $_submenu->position = $sub_position;
                            $_submenu->parent = $_menu->id;
                            $_submenu->type = $_menu->type;
                            $_submenu->save();
                        }
                    }
                }
            }
        }
        return response()->json(true);
    }
}
