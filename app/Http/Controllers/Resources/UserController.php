<?php

namespace App\Http\Controllers\Resources;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Util\Utils;
use Response;
use Gate;

class UserController extends Controller
{

    const FIRST_LOGIN_CREDITS_REWARD = 10;

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $user = User::find($request->id);
        
        abort_unless(Gate::allows('update_profile', auth()->user(), $user), 403, 'Gate');
        
        $user->contact_options = $request->contact_options;
        $user->gender = $request->gender;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->region = $request->region;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->address_alt = $request->address_alt;
        $user->iban = $request->iban;
        $user->bank = $request->bank;
        $user->business_name = $request->business_name;
        $user->website = $request->website;
        $user->commerce_no = $request->commerce_no;
        $user->cui = $request->cui;
        if($user->save())
        {
            return response()->json($user, 200);
        }
        return response()->json(false, 400);
    }

    public function updateProfileAccount(Request $request)
    {
        $user = User::find($request->id);
        $user->name = $request->name;
        if($user->email != $request->email)
        {
            $user->email = $request->email;
            User::sendEmailVerification($user);
        }
        
        if($request->password)
        {
            $this->validate($request, [
                'password' => 'string|min:8',
            ]);
            $user->password = bcrypt($request->password);
        }

        if($user->save())
        {
            return response()->json($user, 200);
        }
        return response()->json(false, 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(null, 204);
    }

    private function validator(Request $request, $id = null)
    {
        $emailValidation = 'required|max:191|email|unique:users';

        $request->validate([
            'name' => 'required|max:191',
            'email' => $emailValidation,
            'password' => 'sometimes|min:6|confirmed',
        ]);
    }

    public function updateProfileAvatar(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|file|image|max:2048',
        ]);
        $user = auth()->user();

        if($user === null)
        {
            return response()->json(['error' => 'not_found', 'message' => 'Not logged in'], 403);
        }
        
        $newPath = $request->photo->store('users/profile', 'public');

        $user->photo_url = $newPath;

        if($user->save())
        {
            return response()->json($user, 200);
        }
        
        return response()->json(['error' => 'generic'], 500);
    }

    public function removeProfileAvatar (Request $request)
    {
        $user = auth()->user();

        if($user === null)
        {
            return response()->json(['error' => 'not_found', 'message' => 'Not logged in'], 403);
        }
       
        $user->photo_url = null;

        if($user->save())
        {
            return response()->json($user, 200);
        }
        
        return response()->json(['error' => 'store', 'message' => 'Database error'], 500);
    }

    public function firstLoginPersonalData(Request $request)
    {
        $user = auth()->user();
        if(!$user->first_login)
        {
            return response()->json(['error' => 'not_allowed', 'message' => 'You missed this opportunity. Contact Admin.'], 403);
        }

        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'region' => 'required',
            'city'  => 'required',
            'address' => 'required', 
        ]);

        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->region = $request->region;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->credits += self::FIRST_LOGIN_CREDITS_REWARD;
        $user->first_login = 0;

        if($user->save())
        {
            return response()->json(['success' => true, 'message' => 'Thank you for your time. '. self::FIRST_LOGIN_CREDITS_REWARD . ' credits have been added to your account']);
        }

        return response()->json(['error' => 'db_too_heavy', 'message' => 'Database is too crowded. Try again later'], 500);
    }

    public function firstLoginCompanyData(Request $request)
    {
        $user = auth()->user();
        if(!$user->first_login)
        {
            return response()->json(['error' => 'not_allowed', 'message' => 'You missed this opportunity. Contact Admin.'], 403);
        }

        $this->validate($request, [
            'business_name' => 'required',
            'cui' => 'required',
            'commerce_no' => 'required',
            'region' => 'required',
            'city'  => 'required',
            'address' => 'required', 
        ]);

        $user->business_name = $request->business_name;
        $user->cui = $request->cui;
        $user->commerce_no = $request->commerce_no;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->credits += self::FIRST_LOGIN_CREDITS_REWARD;
        $user->first_login = 0;
        
        if($user->save())
        {
            return response()->json(['success' => true, 'message' => 'Thank you for your time. '. self::FIRST_LOGIN_CREDITS_REWARD . ' credits have been added to your account']);
        }

        return response()->json(['error' => 'db_too_heavy', 'message' => 'Database is too crowded. Try again later'], 500);
    }

    public function firstLoginRefuse() 
    {
        $user = auth()->user();
        $user->first_login = 0;

        if($user->save())
        {
            return response()->json(['success' => true, 'message' => 'See you around']);
        }

        return response()->json(['error' => 'db_too_heavy', 'message' => 'Database is too crowded. Try again later'], 500);
    }
}
