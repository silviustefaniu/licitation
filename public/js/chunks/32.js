(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[32],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store_cms_moduleCms_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/store/cms/moduleCms.js */ "./resources/js/src/store/cms/moduleCms.js");
/* harmony import */ var _PageView_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PageView.vue */ "./resources/js/src/views/apps/cms/PageView.vue");
/* harmony import */ var _PageMetadata_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PageMetadata.vue */ "./resources/js/src/views/apps/cms/PageMetadata.vue");
/* harmony import */ var _PageSettings_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./PageSettings.vue */ "./resources/js/src/views/apps/cms/PageSettings.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// Store Module




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    PageView: _PageView_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    PageMetadata: _PageMetadata_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    PageSettings: _PageSettings_vue__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  data: function data() {
    return {
      page_data: null,
      page_not_found: false,
      page_settings: null
    };
  },
  watch: {},
  methods: {
    capitalize: function capitalize(str) {
      return str.slice(0, 1).toUpperCase() + str.slice(1, str.length);
    },
    sending_event: function sending_event(event) {
      var _this = this;

      var photo = event.target.files[0];
      var id = this.page_data.id;
      var data = new FormData();
      data.append('photo', photo);
      data.append('id', id);
      this.$store.dispatch('moduleCms/uploadShortcode', data).then(function (data) {
        _this.page_data.shortcodes[0].content = data.data;
        _this.fileUploaded = true;
      }).catch(function (error) {});
    }
  },
  computed: {},
  created: function created() {
    var _this2 = this;

    if (!_store_cms_moduleCms_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered) {
      this.$store.registerModule('moduleCms', _store_cms_moduleCms_js__WEBPACK_IMPORTED_MODULE_0__["default"]);
      _store_cms_moduleCms_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered = true;
    }

    this.$vs.loading();
    var pageId = this.$route.params.pageId;
    this.$store.dispatch('moduleCms/fetchPage', pageId).then(function (res) {
      _this2.page_data = res.data;

      _this2.$vs.loading.close();
    }).catch(function (error) {
      if (error.response.status === 404) {
        _this2.page_not_found = pageId != 0;
        return;
      }

      _this2.$vs.loading.close();

      _this2.$vs.notify({
        title: 'Error',
        text: error.response.data.message,
        iconPack: 'feather',
        icon: 'icon-alert-circle',
        color: 'danger'
      });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=template&id=7af6377c&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=template&id=7af6377c& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.page_data
    ? _c("div", [
        _c("div", { staticClass: "w-10/12 vs-col" }, [
          _c(
            "div",
            { attrs: { id: "page-page-edit" } },
            [
              _c(
                "vs-alert",
                {
                  attrs: {
                    color: "danger",
                    title: "Page Not Found",
                    active: _vm.page_not_found
                  },
                  on: {
                    "update:active": function($event) {
                      _vm.page_not_found = $event
                    }
                  }
                },
                [
                  _c("span", [
                    _vm._v(
                      "User record with id: " +
                        _vm._s(_vm.$route.params.pageId) +
                        " not found. "
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "span",
                    [
                      _c("span", [_vm._v("Check ")]),
                      _c(
                        "router-link",
                        {
                          staticClass: "text-inherit underline",
                          attrs: { to: { name: "page-page-list" } }
                        },
                        [_vm._v("All Users")]
                      )
                    ],
                    1
                  )
                ]
              ),
              _vm._v(" "),
              _vm.page_data
                ? _c("vx-card", [
                    _c(
                      "div",
                      {
                        staticClass: "tabs-container px-6 pt-6",
                        attrs: { slot: "no-body" },
                        slot: "no-body"
                      },
                      [
                        _c(
                          "vs-tabs",
                          { staticClass: "tab-action-btn-fill-conatiner" },
                          [
                            _c(
                              "vs-tab",
                              {
                                attrs: {
                                  label: "Page constructor",
                                  "icon-pack": "feather",
                                  icon: "icon-grid"
                                }
                              },
                              [
                                _c(
                                  "div",
                                  { staticClass: "tab-text" },
                                  [
                                    _c("page-view", {
                                      staticClass: "mt-4",
                                      attrs: { data: _vm.page_data }
                                    })
                                  ],
                                  1
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "vs-tab",
                              {
                                attrs: {
                                  label: "Metadata",
                                  "icon-pack": "feather",
                                  icon: "icon-info"
                                }
                              },
                              [
                                _c(
                                  "div",
                                  { staticClass: "tab-text" },
                                  [
                                    _c("page-metadata", {
                                      staticClass: "mt-4",
                                      attrs: { data: _vm.page_data }
                                    })
                                  ],
                                  1
                                )
                              ]
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ])
                : _vm._e()
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "w-2/12 vs-col" },
          [
            _vm.page_data
              ? _c("page-settings", {
                  attrs: { page_data: _vm.page_data, is_new: false }
                })
              : _vm._e()
          ],
          1
        )
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/apps/cms/PageEdit.vue":
/*!******************************************************!*\
  !*** ./resources/js/src/views/apps/cms/PageEdit.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PageEdit_vue_vue_type_template_id_7af6377c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PageEdit.vue?vue&type=template&id=7af6377c& */ "./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=template&id=7af6377c&");
/* harmony import */ var _PageEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PageEdit.vue?vue&type=script&lang=js& */ "./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PageEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PageEdit_vue_vue_type_template_id_7af6377c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PageEdit_vue_vue_type_template_id_7af6377c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/apps/cms/PageEdit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PageEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageEdit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PageEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=template&id=7af6377c&":
/*!*************************************************************************************!*\
  !*** ./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=template&id=7af6377c& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PageEdit_vue_vue_type_template_id_7af6377c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageEdit.vue?vue&type=template&id=7af6377c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/apps/cms/PageEdit.vue?vue&type=template&id=7af6377c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PageEdit_vue_vue_type_template_id_7af6377c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PageEdit_vue_vue_type_template_id_7af6377c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);