<?php

use Database\traits\TruncateTable;
use Database\traits\DisableForeignKeys;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;

class UsersSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {

        // Users pagination
        // I know how to use factories, I just wanted to add it.
        $people = [
            'Alefe Souza',
            'Ada Lovelace', 'Charles Babbage', 'George Boole', 'Alan Turing', 'Dennis Ritchie',
            'Rasmus Lerdorf', 'Brendan Eich', 'James Gosling', 'Anders Hejlsberg', 'Rob Pike', 'Ken Thompson',
            'Margareth Hamilton', 'Grace Hooper',
            'Bill Gates', 'Steve Jobs', 'Linus Torvalds',
            'Taylor Otwell', 'Evan You', 'Miguel de Icaza', 'James Montemagno',
        ];

        $managers = [
            'manager', 'manager1'
        ];
        $superadmins = [
            'superadmin', 'kakiburu'
        ];
        $admins = [
            'admin', 'cadir'
        ];

        $role = new Role();
        $role->name = 'user';
        $role->weight = 1;
        $role->permissions = null;
        $role->save();
        foreach ($people as $person) {
            $user = User::create([
                'name' => $person,
                'email' => strtolower(str_replace(' ', '.', $person)).'@info.com',
                'password' => bcrypt('password'),
                'credits' => rand(20.0,40000.0),
                'phone' => '07'. rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9),
                'country' => 'Romania',
                'city' => 'Bucuresti',
                'gender' => rand(0,2),
                'region' => 'If',
                'address' => 'Str. Random Nr' . rand(1,44). ' T'.rand(0,9) . ' A.' . rand(1,100),
                'business_name' => 'Business',
                'website' => 'www.example.com',
                'commerce_no' => 'Test',
                'iban' => 'ROXXXXXXXXXXXXXXXXXXX',
                'bank' => rand(2,4) % 2 == 0 ? 'Transilvania' : 'ING',
                'contact_options' => ['phone', 'email'],
                'email_verified_at' => Carbon::now(),
                'cui' => 'test',
                'current_role' => $role->id
            ]);
            $user->roles()->attach($role);
        }
        $role = new Role();
        $role->name = 'manager';
        $role->weight = 1;
        $role->permissions = collect(Role::PERMISSIONS)->collapse()->toArray();
        $role->save();
        foreach ($managers as $person) {
            User::create([
                'name' => $person,
                'email' => strtolower(str_replace(' ', '.', $person)).'@info.com',
                'password' => bcrypt('password'),
                'credits' => rand(20.0,40000.0),
                'phone' => '07'. rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9),
                'country' => 'Romania',
                'city' => 'Bucuresti',
                'region' => 'If',
                'address' => 'Str. Random Nr' . rand(1,44). ' T'.rand(0,9) . ' A.' . rand(1,100),
                'business_name' => 'Business',
                'commerce_no' => 'Test',
                'contact_options' => ['phone', 'email'],
                'email_verified_at' => Carbon::now(),
                'cui' => 'test',
                'current_role' => $role->id
            ]);
            $user->roles()->attach($role);
            
        }
        $role = new Role();
        $role->name = 'superadmin';
        $role->weight = 1;
        $role->permissions = collect(Role::PERMISSIONS)->collapse()->toArray();
        $role->save();
        foreach ($superadmins as $person) {
            User::create([
                'name' => $person,
                'email' => strtolower(str_replace(' ', '.', $person)).'@info.com',
                'password' => bcrypt('password'),
                'credits' => rand(20.0,40000.0),
                'phone' => '07'. rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9),
                'country' => 'Romania',
                'city' => 'Bucuresti',
                'region' => 'If',
                'address' => 'Str. Random Nr' . rand(1,44). ' T'.rand(0,9) . ' A.' . rand(1,100),
                'business_name' => 'Business',
                'commerce_no' => 'Test',
                'contact_options' => ['phone', 'email'],
                'email_verified_at' => Carbon::now(),
                'cui' => 'test',
                'current_role' => $role->id
            ]);
            $user->roles()->attach($role);
        }
        
        $role = new Role();
        $role->name = 'admin';
        $role->weight = 1;
        $role->permissions = collect(Role::PERMISSIONS)->collapse()->toArray();
        $role->save();
        foreach ($admins as $person) {
            User::create([
                'name' => $person,
                'email' => strtolower(str_replace(' ', '.', $person)).'@info.com',
                'password' => bcrypt('password'),
                'credits' => rand(20.0,40000.0),
                'phone' => '07'. rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9),
                'country' => 'Romania',
                'city' => 'Bucuresti',
                'region' => 'If',
                'address' => 'Str. Random Nr' . rand(1,44). ' T'.rand(0,9) . ' A.' . rand(1,100),
                'business_name' => 'Business',
                'commerce_no' => 'Test',
                'contact_options' => ['phone', 'email'],
                'email_verified_at' => Carbon::now(),
                'cui' => 'test',
                'current_role' => $role->id
            ]);
            $user->roles()->attach($role);
        }
    }
}
