<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mail;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    protected $with = ['roles'];
    protected $appends = [
        'home_path',
        'user_role',
        'status',
        'is_verified',
        'acl_role',
        'local_roles',
        'permissions',
        'full_name',
    ];

    protected $casts = [
        'contact_options' => 'array',
    ];

    protected $fillable = [
        'name', 'surname', 'email', 'password', 'access_token', 'credits', 'customer_type' , 'phone',
        'region', 'city', 'address', 'photo_url', 
        'iban', 'bank', 'business_name', 'commerce_no', 'cui', 
        'active', 'confirmation_code', 'confirmed', 'email_verified_at', 'email_token' , 'deleted_at',
        'sms_code', 'phone_verified_at',
        'current_role',
        'first_login',
    ];
    
    CONST CSV_COLUMNS = [
        'name', 'email', 'credits', 'customer_type' , 'phone',
        'region', 'city', 'address', 'photo_url', 
        'iban', 'bank', 'business_name', 'commerce_no', 'cui', 
        'active', 'confirmed' ,
    ];

    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at',
    ];

    const GENDER = [
        0 => 'Male',
        1 => 'Female',
        2 => 'Other',
    ];

    CONST DEFAULT_PICTURE_URL = 'images/profile/default-user.jpg';

    /**
     * Relation with role
     *
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles', 'user_id', 'role_id');
    }

    public function scopeNotAdmin($query)
    {
        return $query->whereHas('roles', function($queryx){
            return $queryx->whereIn('id', [Role::ADMIN, Role::SUPERADMIN]);
        }, '=', 0);
    }
    public function scopeNotSuperAdmin($query)
    {
        return $query->whereHas('roles', function($queryx){
            return $queryx->where('id', Role::SUPERADMIN);
        }, '=', 0);
    }

    public function getFullNameAttribute()
    {
        if($this->name)
        {
            if($this->surname)
            {
                return $this->name . ' ' . $this->surname;
            }
            return $this->name;
        }
        if($this->business_name)
        {
            return $this->business_name;
        }
        return '';
    }

    public function getUserRoleAttribute()
    {
        $role = Role::find($this->current_role);
        if($role === null)
        {
            return 'public';
        }
        if(Role::SUPERADMIN == $role->id)
        {
            return 'superadmin';
        }
        if(Role::ADMIN == $role->id)
        {
            return 'admin';
        }
        if('manager' == $role->name)
        {
            return 'manager';
        }
        
        if($role !== null)
        {
            return $role->name;
        }
        return 'user';
    }

    public function getPermissionsAttribute()
    {
        $role = Role::find($this->current_role);
        if($role === null)
        {
            return [];
        }
        return $role->permissions;
    }

    public function isAdmin()
    {
        return $this->user_role == 'admin';
    }

    public function isSuperAdmin()
    {
        return $this->user_role == 'superadmin';
    }

    public function getAclRoleAttribute()
    {
        $role = Role::find($this->current_role);
        if($role === null || $role->name == 'user')
        {
            return 'user';
        }
        if($role->permissions != null && $role->permissions != [])
        {
            return 'manager';
        }
    }

    public function getStatusAttribute()
    {
        return (isset($this->attributes['deleted_at']) && $this->attributes['deleted_at']) === null ? (Carbon::parse($this->updated_at)->toDateTimeString() > Carbon::now()->subMinutes(1) ? 'online' : 'offline') : 'banned';  
    }

    public function isVerified()
    {
        return $this->email_verified_at === null ? false : true;
    }

    public function getIsVerifiedAttribute()
    {
        return $this->isVerified() ? 'verificat' : 'neverificat';  
    }

    public function getPhotoUrlAttribute()
    {
        if($this->attributes['photo_url'] === null)
        {
            return asset(self::DEFAULT_PICTURE_URL);
        }
        return asset('storage/'.$this->attributes['photo_url']); 
    }

    public function getHomePathAttribute()
    {
        switch ($this->type_id) {
            case 1:
            return '/';
            default:
            return '/'; // TODO change
        }
    }

    public function getLocalRolesAttribute()
    {
        $data = [];
        $roles = Role::whereNotIn('id', Role::ADMINS)->pluck('name')->toArray();
        foreach($roles as $role)
        {
            $data[] = [
                'label' => ucfirst($role),
                'value' => $role,
            ];
        }
        return $data;
    }

    public function touch($save = true)
    {
        $this->last_login = Carbon::now();
        if($save)
        {
            $this->save();
        }
    }

    public function hasVerifiedEmail()
    {
        return $this->email_verified_at !== null;
    }

    public function hasPermission($permission)
    {
        
        $role = Role::find($this->current_role);
        if($role === null)
        {
            abort(404, 'Role Not Found');
        }
        if($role->id == Role::SUPERADMIN)
        {
            return true;
        }
        return in_array($permission, $role->permissions);
        
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    static public function sendEmailVerification(&$user)
    {
        $subject = "Please verify your email address.";
        $verification_code = generateRandomString(30);
        $user->email_token = $verification_code;
        $user->email_verified_at = null;
        Mail::send('email.verify', ['name' => $user->name, 'verification_code' => $verification_code],
            function($mail) use ($user, $subject){
                $mail->from(config('mail.from.address'), config('mail.from.name'));
                $mail->to($user->email, $user->name);
                $mail->subject($subject);
            });
    }
}
