import auth from '@/auth/authService'

export default {
  isUserLoggedIn: () => {
    let isAuthenticated = false

    if (auth.isAuthenticated()) isAuthenticated = true

    return isAuthenticated
  }
}
