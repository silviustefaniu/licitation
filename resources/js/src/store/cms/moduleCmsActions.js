/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {
  fetchFrontMenus({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get("/api/home/menus")
        .then((response) => {
          commit('SET_FRONT_MENUS', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchFrontPage({ commit }, url) {
    return new Promise((resolve, reject) => {
      axios.get("/api/home/pages/"+url)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchPages({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get("/api/pages/index")
        .then((response) => {
          commit('SET_PAGES', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchPage({ commit }, pageId) {
    return new Promise((resolve, reject) => {
      axios.get(`/api/pages/${pageId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchMenus({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get("/api/menus/index")
        .then((response) => {
          commit('SET_MENUS', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchMenu({ commit }, menuId) {
    return new Promise((resolve, reject) => {
      axios.get(`/api/menus/${menuId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  addMenu({ commit }, menu) {
    return new Promise((resolve, reject) => {
      axios.post("/api/menus/create",
        {
          name: menu.name,
          url: menu.url,
          blank: menu.blank
        })
        .then((response) => {
          //commit('ADD_MENU', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  addPage({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios.post("/api/pages/create",
        {
          name: item.name,
          url: item.url,
          metadata: item.metadata,
          parent: item.parent,
          shortcodes: item.shortcodes,
          visible: item.visible,
          classes: null,
        })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editPage({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios.post("/api/pages/edit", {
        id: item.id,
        name: item.name,
        url: item.url,
        metadata: item.metadata,
        shortcodes: item.shortcodes,
        visible: item.visible,
        classes: null,
      })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  removeMenu({ commit }, id) {
    return new Promise((resolve, reject) => {
      axios.post("/api/menus/delete", { id: id })
        .then((response) => {
          commit('REMOVE_MENU', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  removePage({ commit }, id) {
    return new Promise((resolve, reject) => {
      axios.post("/api/pages/delete", { id: id })
        .then((response) => {
          commit('REMOVE_PAGE', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  pageVisibility({ commit }, menu) {
    return new Promise((resolve, reject) => {
      axios.post("/api/pages/make-visible", { id: menu.id, visible: menu.visible })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  arrangeMenus({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post("/api/menus/arrange", { data: data })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  addShortcode({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios.post("/api/shortcodes/store",
        {
          name: item.name,
          classes: item.classes,
          content: item.content,
          position: item.position,
          parent: item.parent,
          visible: item.visible,
          type: item.type,
        })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  editShortcode({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios.post("/api/shortcodes/edit", {
        id: item.id,
        name: item.name,
        classes: item.classes,
        content: item.content,
        position: item.position,
        parent: item.parent,
        visible: item.visible,
        type: item.type,
      })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  removeShortcode({ commit }, id) {
    return new Promise((resolve, reject) => {
      axios.delete("/api/shortcodes/delete", { id: id })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  arrangeShortcodes({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.delete("/api/shortcodes/arrange", { data: data })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  uploadShortcode({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post("/api/shortcodes/upload", data)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
}
