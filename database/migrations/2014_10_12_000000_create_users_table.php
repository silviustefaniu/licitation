<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->json('contact_options')->nullable();
            $table->string('birthday')->nullable();
            $table->string('photo_url')->nullable();
            $table->tinyInteger('gender')->default(2)->comment('0-Male _ 1-Female _ 2-Other');
            $table->float('credits')->default(0);
            $table->tinyInteger('customer_type')->default(0);
            $table->string('phone')->nullable();
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('address_alt')->nullable();
            $table->string('iban')->nullable();
            $table->string('bank')->nullable();
            $table->string('business_name')->nullable();
            $table->string('website')->nullable();
            $table->string('commerce_no')->nullable()->comment('nr reg comertului');
            $table->string('cui')->nullable()->comment('cui/cif');
            $table->tinyInteger('active')->default(1)->unsigned();
            $table->uuid('confirmation_code')->nullable();
            $table->boolean('confirmed')->default(config('access.users.confirm_email') ? false : true);
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('access_token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
