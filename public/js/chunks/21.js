(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[21],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/store/user-management/moduleUserManagement.js */ "./resources/js/src/store/user-management/moduleUserManagement.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // Store Module


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      business_name: '',
      cui: '',
      commerce_no: '',
      region: '',
      city: '',
      address: '',
      isTermsConditionAccepted: false,
      judete: ['Alba', 'Bucuresti', 'Timis', 'Vrancea'],
      orase: ['Alba', 'Bucuresti', 'Timis', 'Vrancea']
    };
  },
  components: {
    'v-select': vue_select__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  computed: {
    activeUserInfo: function activeUserInfo() {
      return this.$store.state.AppActiveUser;
    },
    validateForm: function validateForm() {
      return !this.errors.any() && this.business_name !== '' && this.cui !== '' && this.commerce_no !== '' && this.region !== '' && this.city !== '' && this.address !== '' && this.isTermsConditionAccepted === true;
    }
  },
  mounted: function mounted() {
    if (this.activeUserInfo.first_login == 0) {
      this.$router.push('/');
    }
  },
  methods: {
    save: function save() {
      var _this = this;

      if (!this.validateForm) return;
      this.$vs.loading();
      var payload = {
        business_name: this.business_name,
        cui: this.cui,
        commerce_no: this.commerce_no,
        region: this.region,
        city: this.city,
        address: this.address
      };
      this.$store.dispatch('userManagement/firstLoginCompanyData', payload).then(function (response) {
        _this.$vs.loading.close();

        _this.$vs.notify({
          title: 'Success',
          text: '10 Credite au fost adaugate in contul tau.',
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'success'
        });

        _this.$router.push('/index-user-felicitari');
      }).catch(function (error) {
        _this.$vs.loading.close();

        _this.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });
      });
    }
  },
  created: function created() {
    // Register Module UserManagement Module
    if (!_store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered) {
      this.$store.registerModule('userManagement', _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_1__["default"]);
      _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered = true;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=template&id=8589ef02&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=template&id=8589ef02& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "h-screen flex w-full bg-img" }, [
    _c(
      "div",
      { staticClass: "vx-col w-10/12 m-4 p-2 ml-auto mr-auto pagina_simpla" },
      [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "contact-formular" }, [
          _c(
            "div",
            { staticClass: "formular xl:w-8/12 w-10/12 ml-auto mr-auto" },
            [
              _c(
                "div",
                { staticClass: "centerx labelx" },
                [
                  _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    staticClass: "w-full formular-entry",
                    attrs: {
                      "data-vv-validate-on": "blur",
                      name: "business_name",
                      label: "NUME COMPANIE"
                    },
                    model: {
                      value: _vm.business_name,
                      callback: function($$v) {
                        _vm.business_name = $$v
                      },
                      expression: "business_name"
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "formular-msg-eroare" }, [
                    _vm._v(_vm._s(_vm.errors.first("business_name")))
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "centerx labelx" },
                [
                  _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    staticClass: "w-full formular-entry",
                    attrs: {
                      "data-vv-validate-on": "blur",
                      name: "cui",
                      label: "CUI/CIF"
                    },
                    model: {
                      value: _vm.cui,
                      callback: function($$v) {
                        _vm.cui = $$v
                      },
                      expression: "cui"
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "formular-msg-eroare" }, [
                    _vm._v(_vm._s(_vm.errors.first("cui")))
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "centerx labelx" },
                [
                  _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    staticClass: "w-full formular-entry",
                    attrs: {
                      "data-vv-validate-on": "blur",
                      name: "commerce_no",
                      label: "NR. REGISTRUL COMERTULUI"
                    },
                    model: {
                      value: _vm.commerce_no,
                      callback: function($$v) {
                        _vm.commerce_no = $$v
                      },
                      expression: "commerce_no"
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "formular-msg-eroare" }, [
                    _vm._v(_vm._s(_vm.errors.first("commerce_no")))
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "centerx labelx smart-select" },
                [
                  _c("label", [_vm._v("Judet")]),
                  _vm._v(" "),
                  _c("v-select", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    attrs: {
                      placeholder: "alegeti...",
                      options: _vm.judete,
                      "data-vv-validate-on": "blur"
                    },
                    model: {
                      value: _vm.region,
                      callback: function($$v) {
                        _vm.region = $$v
                      },
                      expression: "region"
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "formular-msg-eroare" }, [
                    _vm._v(_vm._s(_vm.errors.first("region")))
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "centerx labelx smart-select" },
                [
                  _c("label", [_vm._v("Oras")]),
                  _vm._v(" "),
                  _c("v-select", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    attrs: {
                      placeholder: "alegeti...",
                      options: _vm.orase,
                      "data-vv-validate-on": "blur"
                    },
                    model: {
                      value: _vm.city,
                      callback: function($$v) {
                        _vm.city = $$v
                      },
                      expression: "city"
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "formular-msg-eroare" }, [
                    _vm._v(_vm._s(_vm.errors.first("city")))
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "centerx labelx mt-8" }, [
                _c("label", [_vm._v("Adresa")]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.address,
                      expression: "address"
                    },
                    {
                      name: "validate",
                      rawName: "v-validate",
                      value: "required",
                      expression: "'required'"
                    }
                  ],
                  attrs: { "data-vv-validate-on": "blur", rows: "7" },
                  domProps: { value: _vm.address },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.address = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "formular-msg-eroare" }, [
                  _vm._v(_vm._s(_vm.errors.first("address")))
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "centerx labelx formular-entry-box formular-entry"
                },
                [
                  _c("label", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.isTermsConditionAccepted,
                          expression: "isTermsConditionAccepted"
                        }
                      ],
                      attrs: { type: "checkbox" },
                      domProps: {
                        checked: Array.isArray(_vm.isTermsConditionAccepted)
                          ? _vm._i(_vm.isTermsConditionAccepted, null) > -1
                          : _vm.isTermsConditionAccepted
                      },
                      on: {
                        change: function($event) {
                          var $$a = _vm.isTermsConditionAccepted,
                            $$el = $event.target,
                            $$c = $$el.checked ? true : false
                          if (Array.isArray($$a)) {
                            var $$v = null,
                              $$i = _vm._i($$a, $$v)
                            if ($$el.checked) {
                              $$i < 0 &&
                                (_vm.isTermsConditionAccepted = $$a.concat([
                                  $$v
                                ]))
                            } else {
                              $$i > -1 &&
                                (_vm.isTermsConditionAccepted = $$a
                                  .slice(0, $$i)
                                  .concat($$a.slice($$i + 1)))
                            }
                          } else {
                            _vm.isTermsConditionAccepted = $$c
                          }
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", [
                      _vm._v(
                        "\n                        Sunt de acord cu bla bla si pentru ca am dat bifat aici pot apasa si butonul de trimite. \n                        Sunt de acord cu bla bla si pentru ca am dat bifat aici pot apasa si butonul de trimite.\n                        "
                      )
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "clearfix" }),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "my-4 w-full btn-executa",
                  attrs: { disabled: !_vm.validateForm },
                  on: { click: _vm.save }
                },
                [_vm._v("Salveaza si primesti 10 credite!")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "clearfix" }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "formular-help" },
                [
                  _c("router-link", { attrs: { to: "/index-user-zero" } }, [
                    _vm._v(
                      "Renunt la credite! Nu vreau sa furnizez acum aceste date. Doresc sa accesez platforma mai repede."
                    )
                  ])
                ],
                1
              )
            ]
          )
        ])
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h1", [_vm._v("DATE FIRMA")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n              Phasellus vestibulum, nisl iaculis venenatis sodales, eros mi vulputate elit, eget pretium urna libero et dui. \n              Suspendisse sit amet velit malesuada enim porta fermentum. Maecenas velit felis, tristique vel massa non, \n              facilisis suscipit arcu. Etiam vel aliquam justo. Phasellus hendrerit ex eu ante consequat dictum. \n              Nullam ligula metus, lobortis sed tristique sed, pellentesque et quam. Nullam sed arcu nec quam mattis sagittis nec at mi. \n              Vestibulum placerat nunc in scelerisque porttitor. Mauris condimentum magna dapibus semper varius. Quisque at porttitor nulla. \n              Aliquam erat volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. \n              Quisque quis vulputate velit, pellentesque sodales massa. Phasellus ac neque at ex vestibulum tristique eleifend non velit. \n              Vivamus eget pellentesque odio.\n          "
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/front/login/FirstLoginCompany.vue":
/*!************************************************************!*\
  !*** ./resources/js/src/front/login/FirstLoginCompany.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FirstLoginCompany_vue_vue_type_template_id_8589ef02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FirstLoginCompany.vue?vue&type=template&id=8589ef02& */ "./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=template&id=8589ef02&");
/* harmony import */ var _FirstLoginCompany_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FirstLoginCompany.vue?vue&type=script&lang=js& */ "./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _FirstLoginCompany_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _FirstLoginCompany_vue_vue_type_template_id_8589ef02___WEBPACK_IMPORTED_MODULE_0__["render"],
  _FirstLoginCompany_vue_vue_type_template_id_8589ef02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/front/login/FirstLoginCompany.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FirstLoginCompany_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./FirstLoginCompany.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FirstLoginCompany_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=template&id=8589ef02&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=template&id=8589ef02& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FirstLoginCompany_vue_vue_type_template_id_8589ef02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./FirstLoginCompany.vue?vue&type=template&id=8589ef02& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/FirstLoginCompany.vue?vue&type=template&id=8589ef02&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FirstLoginCompany_vue_vue_type_template_id_8589ef02___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FirstLoginCompany_vue_vue_type_template_id_8589ef02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagement.js":
/*!************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagement.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _moduleUserManagementState_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moduleUserManagementState.js */ "./resources/js/src/store/user-management/moduleUserManagementState.js");
/* harmony import */ var _moduleUserManagementMutations_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./moduleUserManagementMutations.js */ "./resources/js/src/store/user-management/moduleUserManagementMutations.js");
/* harmony import */ var _moduleUserManagementActions_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./moduleUserManagementActions.js */ "./resources/js/src/store/user-management/moduleUserManagementActions.js");
/* harmony import */ var _moduleUserManagementGetters_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./moduleUserManagementGetters.js */ "./resources/js/src/store/user-management/moduleUserManagementGetters.js");
/*=========================================================================================
  File Name: moduleUserManagement.js
  Description: Calendar Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/




/* harmony default export */ __webpack_exports__["default"] = ({
  isRegistered: false,
  namespaced: true,
  state: _moduleUserManagementState_js__WEBPACK_IMPORTED_MODULE_0__["default"],
  mutations: _moduleUserManagementMutations_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  actions: _moduleUserManagementActions_js__WEBPACK_IMPORTED_MODULE_2__["default"],
  getters: _moduleUserManagementGetters_js__WEBPACK_IMPORTED_MODULE_3__["default"]
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementActions.js":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementActions.js ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/core-js/promise */ "./node_modules/@babel/runtime/core-js/promise.js");
/* harmony import */ var _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _axios_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/axios.js */ "./resources/js/src/axios.js");


/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

/* harmony default export */ __webpack_exports__["default"] = ({
  createUser: function createUser(_ref, item) {
    var commit = _ref.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/create", item).then(function (response) {
        //commit('ADD_ITEM', Object.assign(item, {id: response.data.id}))
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchUsers: function fetchUsers(_ref2, access_token) {
    var commit = _ref2.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get('/api/users/index').then(function (response) {
        commit('SET_USERS', response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchUser: function fetchUser(context, userId) {
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/users/".concat(userId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  firstLoginCompanyData: function firstLoginCompanyData(_ref3, data) {
    var commit = _ref3.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/first-login-company-data", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  firstLoginPersonalData: function firstLoginPersonalData(_ref4, data) {
    var commit = _ref4.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/first-login-personal-data", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  firstLoginRefuse: function firstLoginRefuse(_ref5) {
    var commit = _ref5.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/first-login-refuse").then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editUser: function editUser(_ref6, data) {
    var commit = _ref6.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/update-info", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editUserAcc: function editUserAcc(_ref7, data) {
    var commit = _ref7.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/update-account", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editProfile: function editProfile(_ref8, data) {
    var commit = _ref8.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/profile", data).then(function (response) {
        commit('UPDATE_USER_INFO', response.data, {
          root: true
        });
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editProfileAcc: function editProfileAcc(_ref9, data) {
    var commit = _ref9.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/profile/account", data).then(function (response) {
        commit('UPDATE_USER_INFO', response.data, {
          root: true
        });
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeRecord: function removeRecord(_ref10, userId) {
    var commit = _ref10.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].delete("/api/users/".concat(userId)).then(function (response) {
        commit('REMOVE_RECORD', userId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  destroyRecord: function destroyRecord(_ref11, userId) {
    var commit = _ref11.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].delete("/api/users/destroy/".concat(userId)).then(function (response) {
        commit('REMOVE_RECORD', userId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteBulk: function deleteBulk(_ref12, ids) {
    var commit = _ref12.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/delete-bulk", {
        ids: ids
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getCsvColumns: function getCsvColumns(_ref13) {
    var commit = _ref13.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].get("/api/get-csv-columns").then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editUserAvatar: function editUserAvatar(_ref14, data) {
    var commit = _ref14.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/avatar", data).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  editProfileAvatar: function editProfileAvatar(_ref15, data) {
    var commit = _ref15.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/profile/avatar", data).then(function (response) {
        commit('UPDATE_USER_INFO', response.data, {
          root: true
        });
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeUserAvatar: function removeUserAvatar(_ref16, id) {
    var commit = _ref16.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/users/remove-avatar", {
        id: id
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeProfileAvatar: function removeProfileAvatar(_ref17) {
    var commit = _ref17.commit;
    return new _babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_0___default.a(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_1__["default"].post("/api/profile/remove-avatar").then(function (response) {
        commit('UPDATE_USER_INFO', response.data, {
          root: true
        });
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementGetters.js":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementGetters.js ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarGetters.js
  Description: Calendar Module Getters
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementMutations.js":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementMutations.js ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  SET_USERS: function SET_USERS(state, users) {
    state.users = users;
  },
  REMOVE_RECORD: function REMOVE_RECORD(state, itemId) {
    var userIndex = state.users.findIndex(function (u) {
      return u.id === itemId;
    });
    state.users.splice(userIndex, 1);
  }
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementState.js":
/*!*****************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementState.js ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarState.js
  Description: Calendar Module State
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  users: []
});

/***/ })

}]);