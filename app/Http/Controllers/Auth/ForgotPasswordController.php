<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Mail;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function forgotPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);
        $user = User::where('email', $request->email)->first();
        if($user === null)
        {
            return response()->json(['error' => 'not_found', 'message' => 'No user with this email found'], 404);
        }
        $user->confirmation_code = Str::uuid();
        $user->save();
        $subject = "Link to reset your password";
        Mail::send('email.reset', ['name' => $request->email, 'uuid' => $user->confirmation_code],
            function($mail) use ($user, $subject){
                $mail->from(config('mail.from.address'), config('mail.from.name'));
                $mail->to($user->email, $user->name);
                $mail->subject($subject);
            });
        return response()->json(['uuid' => $user->confirmation_code]);
    }

    public function verifyResetLink($uuid)
    {
        if(!Str::isUuid($uuid))
        {
            return response()->json(['error' => 'invalid_code', 'message' => 'Invalid code'], 401);
        }
        $user = User::where('confirmation_code', $uuid)->first();
        if($user === null)
        {
            return response()->json(['error' => 'not_found', 'message' => 'User not found'], 404);
        }
        return response()->json(['email' => $user->email]);
    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|min:8',
            'uuid' => 'required'
        ]);
        if(!Str::isUuid($request->uuid))
        {
            return response()->json(['error' => 'invalid_code', 'message' => 'Invalid code'], 401);
        }
        $user = User::where('confirmation_code', $request->uuid)->first();
        if($user === null)
        {
            return response()->json(['error' => 'not_found', 'message' => 'User not found'], 404);
        }
        $user->password = bcrypt($request->password);
        $user->confirmation_code = null;
        $user->save();
    }
}
