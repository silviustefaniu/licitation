/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default {
  SET_MENUS (state, menus) {
    state.menus.header = menus.header
    state.menus.footer = menus.footer
  },
  SET_PAGES (state, pages) {
    state.pages = pages
  },
  SET_FRONT_MENUS (state, menus) {
    state.front_menus = menus
  },
  ADD_MENU (state, data)
  {
    if(typeof state.menus.header == "undefined" || state.menus.header.length == 0)
    {
      state.menus.header = []
    }
    state.menus.header.push(data)
  },
  REMOVE_MENU (state, data) {
    let itemId = data.id;
    let parentId = data.parent;
    let type = data.type;

    // HERE THE MAIN MENU IS DELETED IN PROMISE. SEE NESTEDMENUS.VUE
    /*if(parentId == null || parentId == 0)
    {
      if(type == 'footer')
      {
        let menuIndex = state.menus.footer.findIndex((u) => u.id == itemId)
        state.menus.footer.splice(menuIndex, 1)
      }
      else
      {
        let menuIndex = state.menus.header.findIndex((u) => u.id == itemId)
        state.menus.header.splice(menuIndex, 1)
      }
    }*/
    if(parentId != null && parentId != 0)
    {
      if(type == 'footer')
      {
        state.menus.footer.forEach(function(m) {
          if(m.id == parentId)
          {
            m.siblings = m.siblings.filter(s => s.id != itemId)
          }
        });
      }
      else
      {
        state.menus.header.forEach(function(m) {
          if(m.id == parentId)
          {
            m.siblings = m.siblings.filter(s => s.id != itemId)
          }
        });
      }
    }
  },
  REMOVE_PAGE (state, page) {
    let itemId = page.id
    let pageIndex = state.pages.findIndex((u) => u.id == itemId)
    state.pages.splice(pageIndex, 1)
  },
}
