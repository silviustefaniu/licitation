<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Util\Utils;
use Response;
use Carbon\Carbon;
use Gate;

class RoleController extends Controller
{
    public function index()
    {
        abort_unless(Gate::allows('list_role', auth()->user()), 403, 'Gate');
        if(auth()->user()->isSuperAdmin())
        {
            $roles = Role::where('id', '<>', Role::SUPERADMIN)->get();
        }
        else if(auth()->user()->isAdmin())
        {
            $roles = Role::whereNotIn('id', Role::ADMINS)->get();
        }
        else
        {
            $roles = Role::whereNotIn('id', Role::ADMINS)->where('id', '<>', auth()->user()->current_role)->get();
        }
        
        return response()->json($roles);
    }

    public function view(Request $request)
    {
        if(auth()->user()->isSuperAdmin())
        {
            $role = Role::where('id', '<>', Role::SUPERADMIN)->where('id', $request->id)->first()->toArray();
            $role['roles_modules'] = Role::PERMISSIONS; //full access from constant
            return response()->json($role);
        }
        else if(auth()->user()->isAdmin())
        {
            $role = Role::whereNotIn('id', Role::ADMINS)->where('id',$request->id)->first();
        }
        else
        {
            $role = Role::whereNotIn('id', Role::ADMINS)->where('id', '<>', auth()->user()->current_role)->where('id',$request->id)->first();
        }
        if($role === null)
        {
            return response()->json(['error' => 'not_found', 'message' => 'Role not found.'], 404);
        }

        //abort_unless(Gate::allows('view_role', auth()->user(), $role), 403, 'Gate');
        
        return response()->json($role);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles',
        ]);

        abort_unless(Gate::allows('create_role', auth()->user()), 403, 'Gate');
        
        $role = new Role();
        $role->name = $request->name;
        $role->weight = 50;
        if($role->save())
        {
            return response()->json(['id' => $role->id]);
        }
        return response()->json(['error' => 'generic', 'message' => 'Database could not add this entry'], 500);
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles',
        ]);

        $role = Role::find($request->id);
        if($role === null)
        {
            return response()->json(['error' => 'not_found', 'message' => 'Role not found'], 404);
        }

        abort_unless(Gate::allows('edit_role', auth()->user()), 403, 'Gate');
        
        $role->name = $request->name;
        $role->weight = 50;
        if($role->save())
        {
            return response()->json($role->name);
        }
        return response()->json(['error' => 'generic'], 401);
    }

    public function delete(Request $request)
    {
        $role = Role::find($request->id);
        abort_unless(Gate::allows('delete_role', auth()->user(), $role), 403, 'Gate');
        
        //attach roles to unroles users
        //cannot delete default roles
        //$auth_role = 

        $role->delete();

        return response()->json(null, 204);
    }

    public function storePermission(Request $request)
    {
        
        $role = Role::find($request->id);

        abort_if(in_array($request->permission, $role->permissions), 400, 'already_exists');

        $role->permissions = Role::getPermissionArray($role, $request->permission);
        
        $role->save();

        return response()->json(true);
    }

    public function editPermissions(Request $request)
    {
        $role = Role::find($request->id);
        
        abort_unless(Gate::allows('edit_role', auth()->user()), 403, 'Gate');
        
        //abort_unless(Role::checkPermissionsExists($request->permissions), 401, 'module_not_found');
        $new_permissions = Role::filterPermissions($request->permissions);
        $role->permissions = $new_permissions;
        
        $role->save();

        return response()->json(true);
    }

    public function editPermission(Request $request)
    {
        abort_unless(Gate::allows('edit_role', auth()->user()), 403, 'Gate');
        
        $role = Role::find($request->id);

        $role->permissions = Role::dropPermissionArray($role, $request->old_permission);

        $role->permissions = Role::getPermissionArray($role, $request->permission);
        
        $role->save();

        return response()->json(true);
    }

    public function deletePermission(Request $request)
    {
        abort_unless(Gate::allows('delete_permission', auth()->user(), $role), 403, 'Gate');
        
        $role = Role::find($request->id);

        $role->permissions = Role::dropPermissionArray($role, $request->permission);

        $role->save();

        return response()->json(true, 204);
    }
}
