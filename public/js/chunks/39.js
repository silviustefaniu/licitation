(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[39],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/FirstLoginDelete.vue?vue&type=template&id=dd8bb4ce&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/FirstLoginDelete.vue?vue&type=template&id=dd8bb4ce& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "h-screen flex w-full bg-img" }, [
    _c(
      "div",
      { staticClass: "vx-col w-10/12 m-4 p-2 ml-auto mr-auto pagina_simpla" },
      [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "contact-formular" }, [
          _c(
            "div",
            { staticClass: "formular xl:w-8/12 w-10/12 ml-auto mr-auto" },
            [
              _c(
                "div",
                {
                  staticClass:
                    "centerx labelx formular-entry-box formular-entry"
                },
                [
                  _c("label", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.isTermsConditionAccepted,
                          expression: "isTermsConditionAccepted"
                        }
                      ],
                      attrs: { type: "checkbox" },
                      domProps: {
                        checked: Array.isArray(_vm.isTermsConditionAccepted)
                          ? _vm._i(_vm.isTermsConditionAccepted, null) > -1
                          : _vm.isTermsConditionAccepted
                      },
                      on: {
                        change: function($event) {
                          var $$a = _vm.isTermsConditionAccepted,
                            $$el = $event.target,
                            $$c = $$el.checked ? true : false
                          if (Array.isArray($$a)) {
                            var $$v = null,
                              $$i = _vm._i($$a, $$v)
                            if ($$el.checked) {
                              $$i < 0 &&
                                (_vm.isTermsConditionAccepted = $$a.concat([
                                  $$v
                                ]))
                            } else {
                              $$i > -1 &&
                                (_vm.isTermsConditionAccepted = $$a
                                  .slice(0, $$i)
                                  .concat($$a.slice($$i + 1)))
                            }
                          } else {
                            _vm.isTermsConditionAccepted = $$c
                          }
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", [
                      _vm._v(
                        '\n                            Sunt de acord cu stergerea contului si am luat la cunostinta ce implica acest lucru, \n                            cat si cu cele stipulate in \n                            "Conditii de utilizare", \n                            "Politica de confidentialitate", \n                            "Prelucrarea datelor cu caracter personal"\n                        '
                      )
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "clearfix" }),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "my-4 w-full btn-executa3",
                  on: { click: _vm.deleteAcc }
                },
                [_vm._v("Sterge cont")]
              )
            ]
          )
        ])
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h1", [_vm._v("DATE PERSONALE")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n              Phasellus vestibulum, nisl iaculis venenatis sodales, eros mi vulputate elit, eget pretium urna libero et dui. \n              Suspendisse sit amet velit malesuada enim porta fermentum. Maecenas velit felis, tristique vel massa non, \n              facilisis suscipit arcu. Etiam vel aliquam justo. Phasellus hendrerit ex eu ante consequat dictum. \n              Nullam ligula metus, lobortis sed tristique sed, pellentesque et quam. Nullam sed arcu nec quam mattis sagittis nec at mi. \n              Vestibulum placerat nunc in scelerisque porttitor. Mauris condimentum magna dapibus semper varius. Quisque at porttitor nulla. \n              Aliquam erat volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. \n              Quisque quis vulputate velit, pellentesque sodales massa. Phasellus ac neque at ex vestibulum tristique eleifend non velit. \n              Vivamus eget pellentesque odio.\n          "
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/front/login/FirstLoginDelete.vue":
/*!***********************************************************!*\
  !*** ./resources/js/src/front/login/FirstLoginDelete.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FirstLoginDelete_vue_vue_type_template_id_dd8bb4ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FirstLoginDelete.vue?vue&type=template&id=dd8bb4ce& */ "./resources/js/src/front/login/FirstLoginDelete.vue?vue&type=template&id=dd8bb4ce&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _FirstLoginDelete_vue_vue_type_template_id_dd8bb4ce___WEBPACK_IMPORTED_MODULE_0__["render"],
  _FirstLoginDelete_vue_vue_type_template_id_dd8bb4ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/front/login/FirstLoginDelete.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/front/login/FirstLoginDelete.vue?vue&type=template&id=dd8bb4ce&":
/*!******************************************************************************************!*\
  !*** ./resources/js/src/front/login/FirstLoginDelete.vue?vue&type=template&id=dd8bb4ce& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FirstLoginDelete_vue_vue_type_template_id_dd8bb4ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./FirstLoginDelete.vue?vue&type=template&id=dd8bb4ce& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/FirstLoginDelete.vue?vue&type=template&id=dd8bb4ce&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FirstLoginDelete_vue_vue_type_template_id_dd8bb4ce___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FirstLoginDelete_vue_vue_type_template_id_dd8bb4ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);