<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $casts = [
        'metadata' => 'array',
        'target_type' => 'boolean'
    ];

    protected $appends = [
        'siblings'
    ];

    protected $with = ['page'];
    public function page()
    {
        return $this->belongsTo('App\Page');
    }

    public function getSiblingsAttribute()
    {
        $siblings = self::whereHas('page', function($q){
            return $q->where('visible', 1);
        })->whereNotNull('parent')->where('id', '<>', $this->id)->where('parent', $this->id)->get();
        if($siblings === null)
        {
            return null;
        }
        foreach($siblings as &$sibling)
        {
            if($sibling->getSiblingsAttribute() !== null)
            {
                $sibling->attributes['siblings'] = $sibling->getSiblingsAttribute();
            }
        }
        return $siblings;
    }
}
