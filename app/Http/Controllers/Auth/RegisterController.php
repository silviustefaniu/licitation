<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use JWTAuth;
use Mail;
use Illuminate\Http\Request;
use \Carbon\Carbon;

use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  Request  $data
     * @return \App\User
     */
    public function registerUser(Request $request)
    {
        $data = $request->all();
        $user = User::where('email', $data['email'])->first();
        if($user !== null)
        {
            return response()->json(['error' => 'already_exists', 'message' => 'This email is taken'], 401);
        }
        $role = Role::where('name', 'user')->first();
        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'current_role' => $role->id
        ]);
        $user->roles()->attach($role);
        $credentials = ['email' => $data['email'], 'password' => $data['password']];
        if(!$token = JWTAuth::attempt($credentials))
        {
            return response()->json(['error' => 'invalid_credentials', 'message' => 'Invalid credentials'], 400);
        }
        
        User::sendEmailVerification($user);

        $user->save();
        return $this->respondWithToken($token);
    }

    public function resendEmail(Request $request)
    {
        $user = User::where('email', $request->email)->first();//auth()->user();
        
        if($user === null)
        {
            return response()->json(['error' => 'not_found', 'message' => 'Not authenticated'], 404);
        }

        if($user->email_verified_at !== null)
        {
            return response()->json(['error' => 'already_verified', 'message' => 'This account is verified. Contact Site Admin'], 400);
        }

        User::sendEmailVerification($user);
        $user->save();
        return response()->json(['message' => 'email_sent']);
    }

    public function verify(Request $request)
    {
        $code = $request->code;
        if(!$code)
        {
            return response()->json(['error' => 'invalid_code', 'message' => 'Invalid Code'], 401);
        }
        $user = User::where('email_token', $code)->first();
        if($user === null)
        {
            return response()->json(['error' => 'invalid_code', 'message' => 'This link has expired'], 401);
        }
        
        if($user->email_token == $code)
        {
            $user->email_verified_at = Carbon::now();
            if($user->save())
            {
                return response()->json(['success' => true, 'email' => $user->email], 200);
            }
        }
        return response()->json(['error' => 'invalid_code', 'message' => 'Invalid code'], 401);
    }

    public function sendSms(Request $request)
    {
        $this->validate($request, [
            'phone' => 'regex:/^[0-9]+$/'
        ]);
        
        $change = false; //$request->change;

        $user = auth()->user();
        if($user === null)
        {
            return response()->json(['error' => 'invalid_code', 'message' => 'This link has expired'], 401);
        }
        if($user->email_verified_at === null)
        {
            return response()->json(['error' => 'email_not_verifid', 'message' => 'Your email is not verified'], 401);
        }

        if($user->phone_verified_at !== null && !$change)
        {
            return response()->json(['error' => 'arleady_verified_sms', 'message' => 'Your account has already been verified. Thank you.'], 401);
        }
        
        $user->phone = $request->phone;
        $user->sms_code = $this->generateSmsCode($user);
        $user->phone_verified_at = null;
        if($user->save())
        {
            return response()->json(['success' => true], 200);
        }
        
    }

    public function verifyPhone(Request $request)
    {
        $this->validate($request, [
            'phone' => 'regex:/^[0-9]+$/',
            'sms_code' => 'required'
        ]);

        $user = auth()->user();
        if($user === null)
        {
            return response()->json(['error' => 'not_found', 'message' => 'Not authenticated'], 404);
        }

        $sms_code = $request->sms_code;

        if($user->sms_code == $sms_code)
        {
            $user->phone_verified_at = Carbon::now();
            $user->save();
            return response()->json(['message' => 'Your phone has been verified'], 200);
        }

        return response()->json(['error' => 'invalid_code', 'message' => 'Invalid Code'], 401);
    }

    private function generateSmsCode($user)
    {
        return '12345';
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'user' => auth()->user(),
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 600
        ])
        ->header('Authorization', $token);
    }

    public function guard()
    {
        return Auth::guard('api');
    }
}
