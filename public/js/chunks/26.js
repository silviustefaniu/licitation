(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[26],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/ResetPassword.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/ResetPassword.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      email: '',
      password: '',
      confirm_password: ''
    };
  },
  mounted: function mounted() {
    var _this = this;

    this.$vs.loading();
    this.$store.dispatch('auth/resetPassword', this.$route.params.uuid).then(function (response) {
      _this.$vs.loading.close();

      _this.email = response.data.email;
    }).catch(function (error) {
      _this.$vs.loading.close();

      _this.$vs.notify({
        title: 'Error',
        text: error.response.data.message,
        iconPack: 'feather',
        icon: 'icon-alert-circle',
        color: 'danger'
      });
    });
  },
  computed: {
    validateForm: function validateForm() {
      return !this.errors.any();
    }
  },
  methods: {
    reset: function reset() {
      var _this2 = this;

      if (!this.validateForm) return;
      this.$vs.loading();
      var data = {
        email: this.email,
        uuid: this.$route.params.uuid,
        password: this.password
      };
      this.$store.dispatch('auth/resetPasswordPost', data).then(function (response) {
        _this2.$vs.loading.close();

        _this2.$vs.notify({
          title: 'Success',
          text: "Password has been reset! You can login now with the new password",
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'success'
        });
      }).catch(function (error) {
        _this2.$vs.loading.close();

        _this2.$vs.notify({
          title: 'Error',
          text: error.response.data.message,
          iconPack: 'feather',
          icon: 'icon-alert-circle',
          color: 'danger'
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/ResetPassword.vue?vue&type=template&id=e4481f36&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/front/login/ResetPassword.vue?vue&type=template&id=e4481f36& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "h-screen flex w-full bg-img vx-row no-gutter items-center justify-center pagina_simpla"
    },
    [
      _c("div", { staticClass: "vx-col w-10/12 m-4 p-2 ml-auto mr-auto" }, [
        _c("h1", [_vm._v("RESETEAZA PAROLA")]),
        _vm._v(" "),
        _c("div", { staticClass: "contact-formular" }, [
          _c(
            "div",
            {
              staticClass:
                "clearfix formular xl:w-8/12 w-10/12 ml-auto mr-auto "
            },
            [
              _c(
                "div",
                { staticClass: "centerx labelx" },
                [
                  _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required|email",
                        expression: "'required|email'"
                      }
                    ],
                    staticClass: "w-full formular-entry",
                    attrs: {
                      "icon-no-border": "",
                      "icon-pack": "fa",
                      icon: "fa-user",
                      "data-vv-validate-on": "blur",
                      name: "email",
                      type: "email",
                      label: "ADRESA EMAIL"
                    },
                    model: {
                      value: _vm.email,
                      callback: function($$v) {
                        _vm.email = $$v
                      },
                      expression: "email"
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "formular-msg-eroare" }, [
                    _vm._v(_vm._s(_vm.errors.first("email")))
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "centerx labelx" },
                [
                  _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required|min:8",
                        expression: "'required|min:8'"
                      }
                    ],
                    ref: "password",
                    staticClass: "w-full formular-entry",
                    attrs: {
                      type: "password",
                      "icon-no-border": "",
                      "icon-pack": "fa",
                      icon: "fa-lock",
                      "data-vv-validate-on": "blur",
                      name: "password",
                      label: "PAROLA noua"
                    },
                    model: {
                      value: _vm.password,
                      callback: function($$v) {
                        _vm.password = $$v
                      },
                      expression: "password"
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "formular-msg-eroare" }, [
                    _vm._v(_vm._s(_vm.errors.first("password")))
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "centerx labelx" },
                [
                  _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "min:8|confirmed:password",
                        expression: "'min:8|confirmed:password'"
                      }
                    ],
                    staticClass: "w-full formular-entry",
                    attrs: {
                      type: "password",
                      "icon-no-border": "",
                      "icon-pack": "fa",
                      icon: "fa-lock",
                      "data-vv-validate-on": "blur",
                      "data-vv-as": "password",
                      name: "confirm_password",
                      label: "REPETA PAROLA"
                    },
                    model: {
                      value: _vm.confirm_password,
                      callback: function($$v) {
                        _vm.confirm_password = $$v
                      },
                      expression: "confirm_password"
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "formular-msg-eroare" }, [
                    _vm._v(_vm._s(_vm.errors.first("confirm_password")))
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "clearfix" }),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "my-4 w-full btn-executa",
                  attrs: { disabled: !_vm.validateForm },
                  on: { click: _vm.reset }
                },
                [_vm._v("reseteaza parola")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "formular-help" },
                [
                  _c("router-link", { attrs: { to: "/faq" } }, [
                    _vm._v("(nu te descurci - vezi indrumari)")
                  ])
                ],
                1
              )
            ]
          )
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/front/login/ResetPassword.vue":
/*!********************************************************!*\
  !*** ./resources/js/src/front/login/ResetPassword.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ResetPassword_vue_vue_type_template_id_e4481f36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ResetPassword.vue?vue&type=template&id=e4481f36& */ "./resources/js/src/front/login/ResetPassword.vue?vue&type=template&id=e4481f36&");
/* harmony import */ var _ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ResetPassword.vue?vue&type=script&lang=js& */ "./resources/js/src/front/login/ResetPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ResetPassword_vue_vue_type_template_id_e4481f36___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ResetPassword_vue_vue_type_template_id_e4481f36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/front/login/ResetPassword.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/front/login/ResetPassword.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/front/login/ResetPassword.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ResetPassword.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/ResetPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/front/login/ResetPassword.vue?vue&type=template&id=e4481f36&":
/*!***************************************************************************************!*\
  !*** ./resources/js/src/front/login/ResetPassword.vue?vue&type=template&id=e4481f36& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_template_id_e4481f36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ResetPassword.vue?vue&type=template&id=e4481f36& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/front/login/ResetPassword.vue?vue&type=template&id=e4481f36&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_template_id_e4481f36___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_template_id_e4481f36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);