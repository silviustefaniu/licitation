<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport implements FromArray, WithHeadings
{
    protected $to_email;
    protected $request_email;
    protected $csv_format;
    protected $rows;
    protected $columns;
    protected $fileName;
    protected $selection_option;
    

    public function __construct($to_email, $request_email, $csv_format, $rows, $columns, $selection_option, $fileName)
    {
        
            $this->rows = $rows;
        
        $this->to_email = $to_email;
        $this->request_email = $request_email;
        $this->csv_format = $csv_format;
        $this->rows = $rows;
        $this->columns = $columns;
        $this->fileName = $fileName;
        $this->selection_option = $selection_option;
    }

    public function array(): array
    {
        foreach($this->rows as &$row)
        {
            foreach($row as $key => $value)
            {
                if(!in_array($key, $this->columns))
                {
                    unset($row[$key]);
                }
            } 
        }
        return array_values($this->rows);
    }

    public function headings(): array
    {
        return array_values($this->columns);
    }

    
}

?>