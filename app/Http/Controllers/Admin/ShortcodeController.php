<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Shortcode;

class ShortcodeController extends Controller
{
    public function store(Request $request)
    {
        $shortcode = new Shortcode();
        $shortcode->name = $shortcode->name;
        $shortcode->classes = $request->classes;
        $shortcode->content = $request->content;
        $shortcode->position = $request->position;
        $shortcode->parent = $request->parent;
        $shortcode->save();

        return response()->json(true);
    }

    public function delete(Request $request)
    {
        Shortcode::where('id', $request->id)->delete();
        return response()->json(true, 204);
    }

    public function arrange(Request $request)
    {
        $data = $request->data;
        foreach($data as $id => $position )
        {
            $menu = Menu::find($id);
            $menu->position = $position;
            $menu->save();
        }
        return response()->json(true);
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|file|image|max:2048',
        ]);
        
        if($newPath = $request->photo->store('website/images', 'public'))
        {
            return response()->json($newPath, 200);
        }
        
        return response()->json(['error' => 'generic'], 401);
    }
}
